# shell2.py
"""Volume 3: Unix Shell 2.
<Name> Brian James
<Class>
<Date>
"""

import os
from glob import glob
import subprocess

# Problem 3
def grep(target_string, file_pattern):
    """Find all files in the current directory or its subdirectories that
    match the file pattern, then determine which ones contain the target
    string.

    Parameters:
        target_string (str): A string to search for in the files whose names
            match the file_pattern.
        file_pattern (str): Specifies which files to search.
    """
    filename_list = []
    for directory, subdirectory, files in os.walk('.'):
        for filename in files:
            if filename.endswith(file_pattern):
                with open(os.path.join(directory, filename),'r') as f:
                    found = False
                    for line in f:
                        for word in line.split():
                            if word == target_string:
                                found = True
                                break
                        if found == True:
                            filename_list.append(filename)
                            break
    return filename_list

# Problem 4
def largest_files(n):
    """Return a list of the n largest files in the current directory or its
    subdirectories (from largest to smallest).
    """
    results = glob("**/*.*",recursive =True)
    sorted_results = sorted(results,key=os.path.getsize)
    sorted_results.reverse()
    sorted_results = sorted_results[:n]
    args = ["wc", "-l"]
    outfile = open("smallest.txt",'w')
    infile = open(sorted_results[-1],'r')
    subprocess.call(args,stdin=infile,stdout=outfile)      # write the smallest line count in the smallest.txt file
    return sorted_results

# Problem 6    
def prob6(n = 10):
   """this problem counts to or from n three different ways, and
      returns the resulting lists each integer
   
   Parameters:
       n (int): the integer to count to and down from
   Returns:
       integerCounter (list): list of integers from 0 to the number n
       twoCounter (list): list of integers created by counting down from n by two
       threeCounter (list): list of integers created by counting up to n by 3
   """
   #print what the program is doing
   integerCounter = list()
   twoCounter = list()
   threeCounter = list()
   counter = n
   for i in range(n+1):
       integerCounter.append(i)
       if (i % 2 == 0):
           twoCounter.append(counter - i)
       if (i % 3 == 0):
           threeCounter.append(i)
   #return relevant values
   return integerCounter, twoCounter, threeCounter
