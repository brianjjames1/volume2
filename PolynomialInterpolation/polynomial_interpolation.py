# polynomial_interpolation.py
"""Volume 2: Polynomial Interpolation.
<Name> Brian James
<Class>
<Date>
"""

import numpy as np
from numpy import fft
from numpy import linalg as la
from matplotlib import pyplot as plt
from scipy.interpolate import BarycentricInterpolator


# Problems 1 and 2
def lagrange(xint, yint, points):
    """Find an interpolating polynomial of lowest degree through the points
    (xint, yint) using the Lagrange method and evaluate that polynomial at
    the specified points.

    Parameters:
        xint ((n,) ndarray): x values to be interpolated.
        yint ((n,) ndarray): y values to be interpolated.
        points((m,) ndarray): x values at which to evaluate the polynomial.

    Returns:
        ((m,) ndarray): The value of the polynomial at the specified points.
    """
    
    L= []
    lagrange_denom = [] # list of all Lagrange functions L_j denominators which corresponds to xint indices
    n = len(xint) # number of interpolated points
    
    for j, x_j in enumerate(xint):
        denominator = np.prod([(x_j - x_k) for x_k in xint if x_j != x_k])
        lagrange_denom.append(denominator)
        
    for j in range(len(lagrange_denom)):        # length is n
        L_j = []    # create a new list for each L_j
        for x in points:        #calculate L_j for each x, total of m times
            numerator = np.product([x - xint[k] for k in range(n) if j != k])
            L_j.append(numerator / lagrange_denom[j])       #L_j for list (x_0,x_1,...,x_m)
        L_j = np.array(L_j)   # convert list into array
        L.append(L_j)   # insert L_j ndarray at index j of Lagrange list
    L = np.array(L)     # convert into np array
    matrix = np.vstack(L)   # nxm array
    
    poly = []
    for x_i in range(len(matrix[0])):
        col = matrix[:,x_i]
        p = np.sum([(yint[j]*col[j]) for j in range(len(yint))])
        poly.append(p)
    
    return np.array(poly)

# Problems 3 and 4
class Barycentric:
    """Class for performing Barycentric Lagrange interpolation.

    Attributes:
        w ((n,) ndarray): Array of Barycentric weights.
        n (int): Number of interpolation points.
        x ((n,) ndarray): x values of interpolating points.
        y ((n,) ndarray): y values of interpolating points.
    """

    def __init__(self, xint, yint):
        """Calculate the Barycentric weights using initial interpolating points.

        Parameters:
            xint ((n,) ndarray): x values of interpolating points.
            yint ((n,) ndarray): y values of interpolating points.
        """
        
        self.xint = xint
        self.yint = yint
        self.n = len(xint)
        self.w = np.ones(self.n)
        # Calculate the carrying capacity of the interval
        C = (np.max(xint) - np.min(xint)) / 4
        
        shuffle = np.random.permutation(self.n-1)
        for j in range(self.n):
            temp = (xint[j] - np.delete(xint, j)) / C
            temp = temp[shuffle]        # Randomize order of product.
            self.w[j] /= np.product(temp)

    def __call__(self, points):
        """Using the calcuated Barycentric weights, evaluate the interpolating polynomial
        at points.

        Parameters:
            points ((m,) ndarray): Array of points at which to evaluate the polynomial.

        Returns:
            ((m,) ndarray): Array of values where the polynomial has been computed.
        """
        p = []
        for x in points:
            denom = x-self.xint;
            num_sum = np.sum([self.w[j]*self.yint[j]/denom[j] for j in range(self.n)])
            denom_sum = np.sum([self.w[j]/denom[j] for j in range(self.n)])
            p.append(num_sum/denom_sum)
        return np.array(p)

    # Problem 4
    def add_weights(self, xint, yint):
        """Update the existing Barycentric weights using newly given interpolating points
        and create new weights equal to the number of new points.

        Parameters:
            xint ((m,) ndarray): x values of new interpolating points.
            yint ((m,) ndarray): y values of new interpolating points.
        """
        m = len(xint)
        new_weights = np.ones(m)
        
        #Randomizer
        shuffle = np.random.permutation(m-1)
        for i in range(m):
            temp = xint[i]-self.xint
            temp = temp[shuffle]        # Randomize order of product
            new_weights[i] /= np.product(temp)
            # update existing weights
            self.w /= self.xint-xint[i]


        # Update class attributes with updates weights and interpolating points
        self.w = np.concatenate((self.w,new_weights),axis=None)
        self.xint = np.concatenate((self.xint,xint),axis=None)
        self.yint = np.concatenate((self.yint,yint),axis=None)
        self.n += m


# Problem 5
def prob5():
    """For n = 2^2, 2^3, ..., 2^8, calculate the error of intepolating Runge's
    function on [-1,1] with n points using SciPy's BarycentricInterpolator
    class, once with equally spaced points and once with the Chebyshev
    extremal points. Plot the absolute error of the interpolation with each
    method on a log-log plot.
    """
    
    
    f = lambda x: 1/(1+25 * x**2)
    domain = np.linspace(-1,1,400)  # function f(x) with 400 equally spaced points to build it
    f_x = f(domain)
    #colors = ['blue','red','green','brown','purple','orange','pink']
    n_list = [2**i for i in range(2,9)]
    spaced_errors = []
    cheby_errors = []
    
    # Equally spaced points
    plt.subplot(121)
    for n in n_list:    # i determines n=2^i
        pts = np.linspace(-1,1,n)
        poly = BarycentricInterpolator(pts)
        y = f(pts)
        poly.set_yi(y)   # set y points
        error = la.norm(f_x-poly(domain),ord=np.inf)  # calculate error ||f(x)-f~(x)||_inf
        spaced_errors.append(error)
    plt.loglog(n_list,spaced_errors)
    plt.title("Equally Spaced Points")
    plt.xlabel("n values")
    plt.ylabel("Absolute error")
        
    # Chebyshev points
    plt.subplot(122)
    for n in n_list:
        pts = np.array([np.cos(j*np.pi/n) for j in range(n+1)])     # Chebyshev extremizers
        poly = BarycentricInterpolator(pts)
        y = f(pts)
        poly.set_yi(y)   # set y points
        error = la.norm(f_x-poly(domain),ord=np.inf) # calculate error ||f(x)-f~(x)||_inf
        cheby_errors.append(error)
    plt.loglog(n_list,cheby_errors)
    plt.title("Chebyshev Extremizers")
    plt.xlabel("n values")
    plt.ylabel("Absolute Error")
    plt.show()

# Problem 6
def chebyshev_coeffs(f, n):
    """Obtain the Chebyshev coefficients of a polynomial that interpolates
    the function f at n points.

    Parameters:
        f (function): Function to be interpolated.
        n (int): Number of points at which to interpolate.

    Returns:
        coeffs ((n+1,) ndarray): Chebyshev coefficients for the interpolating polynomial.
    """
    y_j = np.array([np.cos(j*np.pi/n) for j in range(n+1)])
    f_y = f(y_j)
    f_y = np.append(f_y,f_y[-2:0:-1])
    a_k = (fft.fft(f_y)).real
    a_k[1:n] = a_k[1:n]*2
    a_k = 1/(2*n) * a_k  #rescale
    return a_k[:n+1]   # return n+1 points

# Problem 7
def prob7(n):
    """Interpolate the air quality data found in airdata.npy using
    Barycentric Lagrange interpolation. Plot the original data and the
    interpolating polynomial.

    Parameters:
        n (int): Number of interpolating points to use.
    """
    
    data = np.load("airdata.npy")
    
    fx = lambda a, b, n: .5*(a+b + (b-a) * np.cos(np.arange(n+1) * np.pi / n))
    a, b = 0, 366 - 1/24
    domain = np.linspace(0, b, 8784)
    points = fx(a, b, n)
    temp = np.abs(points - domain.reshape(8784, 1))
    temp2 = np.argmin(temp, axis=0)
    
    poly = BarycentricInterpolator(domain[temp2])
    poly.set_yi(data[temp2])
    
    plt.plot(domain,data)
    plt.plot(domain,poly(domain))
    plt.show()
