# dynamic_programming.py
"""Volume 2: Dynamic Programming.
<Name> Brian James
<Class>
<Date>
"""

import numpy as np
from matplotlib import pyplot as plt


def calc_stopping(N):
    """Calculate the optimal stopping time and expected value for the
    marriage problem.

    Parameters:
        N (int): The number of candidates.

    Returns:
        (float): The maximum expected value of choosing the best candidate.
        (int): The index of the maximum expected value.
    """
    Vn = 0      # Initial probability for t=N
    expectedVals = [0]
    
    for n in range(N-1,0,-1):
        Vn = max(n/(n+1) * Vn + 1/N, Vn)
        expectedVals.append(Vn)
        
    expectedVals.reverse()
    maxVal = max(expectedVals)
    maxIndex = np.argmax(expectedVals)+1
    
    return maxVal, maxIndex

# Problem 2
def graph_stopping_times(M):
    """Graph the optimal stopping percentage of candidates to interview and
    the maximum probability against M.

    Parameters:
        M (int): The maximum number of candidates.

    Returns:
        (float): The optimal stopping percent of candidates for M.
    """
    
    optStops = []
    optProbs = []
    NVals = np.arange(3,M+1)
    
    for N in NVals:
        data = calc_stopping(N)
        optStop = data[1]/N
        optVal = data[0]
        
        optStops.append(optStop)
        optProbs.append(optVal)
        
    plt.subplot(121)
    plt.plot(NVals,optStops)
    plt.title("Stopping Percentages vs N")
    
    plt.subplot(122)
    plt.plot(NVals,optProbs)
    plt.title("Maximum Probability vs N")
    
    plt.show()
    
    return optProbs[-1]

# Problem 3
def get_consumption(N, u=lambda x: np.sqrt(x)):
    """Create the consumption matrix for the given parameters.

    Parameters:
        N (int): Number of pieces given, where each piece of cake is the
            same size.
        u (function): Utility function.

    Returns:
        C ((N+1,N+1) ndarray): The consumption matrix.
    """
    
    w = np.linspace(0,1,N+1)
    
    # Create consumption matrix
    C = np.zeros((1,N+1))
    for i in range(1,N+1):
        rowi = []
        for j in range(N+1):
            if i-j >=0:
                rowi.append(u(w[i]-w[j]))
            else:
                rowi.append(0)
        C = np.vstack((C,np.array(rowi)))
    
    return C

# Problems 4-6
def eat_cake(T, N, B, u=lambda x: np.sqrt(x)):
    """Create the value and policy matrices for the given parameters.

    Parameters:
        T (int): Time at which to end (T+1 intervals).
        N (int): Number of pieces given, where each piece of cake is the
            same size.
        B (float): Discount factor, where 0 < B < 1.
        u (function): Utility function.

    Returns:
        A ((N+1,T+1) ndarray): The matrix where the (ij)th entry is the
            value of having w_i cake at time j.
        P ((N+1,T+1) ndarray): The matrix where the (ij)th entry is the
            number of pieces to consume given i pieces at time j.
    """
    
    A = np.zeros((N+1,T+1))     # Value matrix initialized with 0s
    P = np.zeros((N+1,T+1))     # Policy matrix initialized with 0s

    w = np.linspace(0,1,N+1)
    
    columnT = np.array([u(wi) for wi in w])
    A[:,-1] = columnT

    # Calculate last column of Policy matrix
    P[:,-1] = w
    
    for t in range(T-1,-1,-1):
        CVMatrix = np.zeros((N+1,N+1))
        for i in range(N+1):
            for j in range(N+1):
                if j <= i:
                    CVMatrix[i,j] = u(w[i]-w[j]) + B * A[j,t+1]
                else:
                    CVMatrix[i,j] = 0
            A[i,t] = max(CVMatrix[i])
            minJ = np.argmax(CVMatrix[i])       # Find index of the max value of CVMatrix row
            P[i,t] = w[i] - w[minJ]             # P_it = w_i - w_j where j is minimum J
    
    return A, P

# Problem 7
def find_policy(T, N, B, u=np.sqrt):
    """Find the most optimal route to take assuming that we start with all of
    the pieces. Show a graph of the optimal policy using graph_policy().

    Parameters:
        T (int): Time at which to end (T+1 intervals).
        N (int): Number of pieces given, where each piece of cake is the
            same size.
        B (float): Discount factor, where 0 < B < 1.
        u (function): Utility function.

    Returns:
        ((T,) ndarray): The matrix describing the optimal percentage to
            consume at each time.
    """
    
    A,P = eat_cake(T,N,B,u)
    
    # Get bottom-left diagonal from P to get optimal policy
    policy = []
    for i in range(len(P[0])):
        policy.append(P[-(i+1),i])
        
    # There is no graph_policy() function, so I cannot graph it unless I had more information about the function.
        
    return np.array(policy)