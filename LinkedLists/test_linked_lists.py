#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 09:49:15 2021

@author: James
"""

import linked_lists
import pytest

def test_Node_init():
    with pytest.raises(TypeError) as excinfo:
        linked_lists.Node(object)
    assert excinfo.value.args[0] == "Data is not of type int, float, or str"

@pytest.fixture
def set_up_LinkedList():
    list1 = linked_lists.LinkedList() # create linked list of size 4
    list1.append(1)
    list1.append(2)
    list1.append(3)
    list1.append(4)
    list2 = linked_lists.LinkedList()   # empty linked list
    return list1, list2
    
def test_LinkedList_find(set_up_LinkedList):
    list1, list2 = set_up_LinkedList
    assert list1.find(1) is list1.head
    assert list1.find(2) is list1.head.next
    assert list1.find(3) is list1.head.next.next
    with pytest.raises(ValueError) as excinfo:
        list1.find(5)
    assert excinfo.value.args[0] == "List does not contain data"
    with pytest.raises(ValueError) as excinfo:
        list2.find(4)
    assert excinfo.value.args[0] == "List does not contain data"
    
def test_LinkedList_get(set_up_LinkedList):
    list1, list2 = set_up_LinkedList
    assert list1.get(3) == list1.head.next.next.next
    
def test_LinkedList_str(set_up_LinkedList):
    list1, list2 = set_up_LinkedList
    assert str(list1) == "[1,2,3,4]"
    
def test_LinkedList_remove(set_up_LinkedList):
    list1, list2 = set_up_LinkedList
    list1.remove(3)
    assert str(list1) == "[1,2,4]"
    with pytest.raises(ValueError) as excinfo:
        list2.remove(1)
    assert excinfo.value.args[0] == "List does not contain data"
    
def test_LinkedList_insert(set_up_LinkedList):
    list1, list2 = set_up_LinkedList
    list1.insert(0,0)
    assert str(list1) == "[0,1,2,3,4]"
    list1.insert(5,5)       # append to the end of the list
    assert str(list1) == "[0,1,2,3,4,5]"
    with pytest.raises(IndexError) as excinfo: 
        list1.insert(7,6)       # enter an invalid index
    assert excinfo.value.args[0] == "index must be between 0 and the length of the list"
    list1.insert(1,99)
    assert str(list1) == "[0,99,1,2,3,4,5]"
    
    
@pytest.fixture
def set_up_Deque():
    deque1 = linked_lists.Deque()
    deque1.append(1)
    deque1.append(2)
    deque1.append(3)
    deque1.append(4)
    deque2 = linked_lists.Deque() # empty Deque
    return deque1, deque2

def test_Deque_pops(set_up_Deque):
    deque1, deque2 = set_up_Deque
    assert deque1.pop() == 4
    assert deque1.popleft() == 1
    deque1.appendleft(1)
    assert str(deque1) == "[1,2,3]"
    with pytest.raises(ValueError) as excinfo:
        deque2.pop()
    assert excinfo.value.args[0] == "List must not be empty in order to pop"
    with pytest.raises(ValueError) as excinfo:
        deque2.popleft()
    assert excinfo.value.args[0] == "List must not be empty in order to popleft"
    deque2.append(1)
    deque2.pop()
    assert str(deque2) == "[]"
    deque2.append(2)
    deque2.popleft()
    assert str(deque2) == "[]"
    
def test_Deque_appendleft(set_up_Deque):
    deque1, deque2 = set_up_Deque
    deque1.appendleft(0)
    assert str(deque1) == "[0,1,2,3,4]"
    deque2.appendleft(1)
    assert str(deque2) == "[1]"
    