# linked_lists.py
"""Volume 2: Linked Lists.
<Name> Brian James
<Class> Sec 03
<Date> 9/30/21
"""

import collections

# Problem 1
class Node:
    """A basic node class for storing data."""
    def __init__(self, data):
        """Store the data in the value attribute.
                
        Raises:
            TypeError: if data is not of type int, float, or str.
        """
        if not (isinstance(data,(int,float,str))):
            raise TypeError("Data is not of type int, float, or str")
            
        self.value = data


class LinkedListNode(Node):
    """A node class for doubly linked lists. Inherits from the Node class.
    Contains references to the next and previous nodes in the linked list.
    """
    def __init__(self, data):
        """Store the data in the value attribute and initialize
        attributes for the next and previous nodes in the list.
        """
        Node.__init__(self, data)       # Use inheritance to set self.value.
        self.next = None                # Reference to the next node.
        self.prev = None                # Reference to the previous node.


# Problems 2-5
class LinkedList:
    """Doubly linked list data structure class.

    Attributes:
        head (LinkedListNode): the first node in the list.
        tail (LinkedListNode): the last node in the list.
    """
    def __init__(self):
        """Initialize the head and tail attributes by setting
        them to None, since the list is empty initially.
        """
        self.head = None
        self.tail = None
        self.length = 0

    def append(self, data):
        """Append a new node containing the data to the end of the list."""
        # Create a new node to store the input data.
        new_node = LinkedListNode(data)
        if self.head is None:
            # If the list is empty, assign the head and tail attributes to
            # new_node, since it becomes the first and last node in the list.
            self.head = new_node
            self.tail = new_node
        else:
            # If the list is not empty, place new_node after the tail.
            self.tail.next = new_node               # tail --> new_node
            new_node.prev = self.tail               # tail <-- new_node
            # Now the last node in the list is new_node, so reassign the tail.
            self.tail = new_node
        self.length += 1

    # Problem 2
    def find(self, data):
        """Return the first node in the list containing the data.

        Raises:
            ValueError: if the list does not contain the data.

        Examples:
            >>> l = LinkedList()
            >>> for x in ['a', 'b', 'c', 'd', 'e']:
            ...     l.append(x)
            ...
            >>> node = l.find('b')
            >>> node.value
            'b'
            >>> l.find('f')
            ValueError: <message>
        """
        
        if self.head is None:
            raise ValueError("List does not contain data")
        
        elif self.head.value == data:
            return self.head
        
        else:
            return self.find_helper(self.head.next,data)
        
        
    def find_helper(self,current_node,data):
        """Helper function to the find function.
        
        Raises:
            ValueError: if the list does not contain the data

        Parameters
        ----------
        current_node : TYPE LinkedListNode
            the node in the list we compare the data to. It is recursively called by the parameter current_node.next
        data : TYPE int,float,str
            the value that the find function is searching for, which is a match contained in the LinkedList

        Returns
        -------
        TYPE int,float,str
            DESCRIPTION.

        """
        if current_node is None:
            raise ValueError("List does not contain data")
        elif current_node.value == data:
            return current_node
        else:
            return self.find_helper(current_node.next,data)

    # Problem 2
    def get(self, i):
        """Return the i-th node in the list.

        Raises:
            IndexError: if i is negative or greater than or equal to the
                current number of nodes.

        Examples:
            >>> l = LinkedList()
            >>> for x in ['a', 'b', 'c', 'd', 'e']:
            ...     l.append(x)
            ...
            >>> node = l.get(3)
            >>> node.value
            'd'
            >>> l.get(5)
            IndexError: <message>
        """
        if i < 0:
            raise IndexError("i must be a positive number")
        if i >= self.length:
            raise IndexError("i must be less than the number of nodes")
        current_node = self.head
        
        while i > 0:
            current_node = current_node.next
            i -= 1
        
        return current_node

    # Problem 3
    def __len__(self):
        """Return the number of nodes in the list.

        Examples:
            >>> l = LinkedList()
            >>> for i in (1, 3, 5):
            ...     l.append(i)
            ...
            >>> len(l)
            3
            >>> l.append(7)
            >>> len(l)
            4
        """
        return self.length

    # Problem 3
    def __str__(self):
        """String representation: the same as a standard Python list.

        Examples:
            >>> l1 = LinkedList()       |   >>> l2 = LinkedList()
            >>> for i in [1,3,5]:       |   >>> for i in ['a','b',"c"]:
            ...     l1.append(i)        |   ...     l2.append(i)
            ...                         |   ...
            >>> print(l1)               |   >>> print(l2)
            [1, 3, 5]                   |   ['a', 'b', 'c']
        """
        output = "["
        current_node = self.head
        
        for i in range(0,self.length):
            output += str(current_node.value)
            if i != self.length-1:
                output += ','
            current_node = current_node.next
        output += ']'
        return output

    # Problem 4
    def remove(self, data):
        """Remove the first node in the list containing the data.

        Raises:
            ValueError: if the list is empty or does not contain the data.

        Examples:
            >>> print(l1)               |   >>> print(l2)
            ['a', 'e', 'i', 'o', 'u']   |   [2, 4, 6, 8]
            >>> l1.remove('i')          |   >>> l2.remove(10)
            >>> l1.remove('a')          |   ValueError: <message>
            >>> l1.remove('u')          |   >>> l3 = LinkedList()
            >>> print(l1)               |   >>> l3.remove(10)
            ['e', 'o']                  |   ValueError: <message>
        """
        try:
            target = self.find(data)
            target.prev.next = target.next                     # -/-> target
            target.next.prev = target.prev                   # target <-/-
            self.length -= 1
        except ValueError:
            raise

    # Problem 5
    def insert(self, index, data):
        """Insert a node containing data into the list immediately before the
        node at the index-th location.

        Raises:
            IndexError: if index is negative or strictly greater than the
                current number of nodes.

        Examples:
            >>> print(l1)               |   >>> len(l2)
            ['b']                       |   5
            >>> l1.insert(0, 'a')       |   >>> l2.insert(6, 'z')
            >>> print(l1)               |   IndexError: <message>
            ['a', 'b']                  |
            >>> l1.insert(2, 'd')       |   >>> l3 = LinkedList()
            >>> print(l1)               |   >>> l3.insert(1, 'a')
            ['a', 'b', 'd']             |   IndexError: <message>
            >>> l1.insert(2, 'c')       |
            >>> print(l1)               |
            ['a', 'b', 'c', 'd']        |
        """
        if index < 0 or index > self.length:
            raise IndexError("index must be between 0 and the length of the list")
        elif index == self.length:    # if inserting at the end of the list, simply append
            self.append(data)    
        else:
            new_node = LinkedListNode(data)
            current_node = self.head
            for i in range(self.length):
                if i == index:
                    new_node.next = current_node
                    new_node.prev = current_node.prev
                    current_node.prev = new_node
                    if i != 0:
                        new_node.prev.next = new_node 
                    elif i == 0:      # if inserting at the start of the list, assign head to new_node
                        self.head = new_node
                    self.length += 1
                    break
                else:
                    current_node = current_node.next

# Problem 6: Deque class.
class Deque(LinkedList):
    """
    A double-ended queue object. This object inherits from the LinkedList object. For a deque, the only elements that can be
    accessed or the first and last elements.
    """
    def __init__(self):
        LinkedList.__init__(self)
    
    def pop(self):
        """Remove the last node in the list and return its data. Account for the special
        case of removing the only node in the list. Raise a ValueError if the list is empty."""
        last_node = self.tail
        if self.length == 0:
            raise ValueError("List must not be empty in order to pop")
        elif self.length == 1:
            self.head = None
            self.tail = None
        else:    
            self.tail = last_node.prev
            self.tail.next = None
        self.length -= 1
        return last_node.value
        
    def popleft(self):
        """Remove the first node in the list and return its data. Raise a ValueError if the list is empty."""
        first_node = self.head
        if self.length == 0:
            raise ValueError("List must not be empty in order to popleft")
        elif self.length ==1:
            self.head = None
            self.tail = None
        else:
            self.head = first_node.next
            self.head.prev = None
        self.length -= 1
        return first_node.value
    
    def appendleft(self,data):
        """Insert a new node at the beginning of the list."""
        new_node = LinkedListNode(data)
        if self.head == None:
            self.head = new_node
            self.tail = new_node
        else:
            self.head.prev = new_node
            new_node.next = self.head
            self.head = new_node
        self.length += 1
        
    def remove(*args, **kwargs):
         raise NotImplementedError("Use pop() or popleft() for removal")
         
    def insert(*args, **kwargs):
        raise NotImplementedError("Use append or appendleft for insertion")

# Problem 7
def prob7(infile, outfile):
    """Reverse the contents of a file by line and write the results to
    another file.

    Parameters:
        infile (str): the file to read from.
        outfile (str): the file to write to.
    """
    lines = []
    reversed_lines = []
    with open(infile) as read_file:
        lines = read_file.readlines()
        
    for i in range(len(lines)):
        reversed_lines.append(lines.pop())      # pop everthing from the text file starting from the end to the beginning
        
    with open(outfile,'w') as write_file:
        write_file.writelines(reversed_lines)