# object_oriented.py
"""Python Essentials: Object Oriented Programming.
<Name> Brian James
<Class> Sec 3
<Date> 9/16/2021
"""


class Backpack:
    """A Backpack object class. Has a name and a list of contents.

    Attributes:
        name (str): the name of the backpack's owner.
        contents (list): the contents of the backpack.
    """

    # Problem 1: Modify __init__() and put(), and write dump().
    def __init__(self, name, color, max_size=5):
        """Set the name and initialize an empty list of contents.

        Parameters:
            name (str): the name of the backpack's owner.
            color (str): the color of the backpack.
            max_size (int): the max number of items the backpack can carry.
            contents (list[obj]): the list of items contained inside the backpack.
        """
        self.name = name
        self.color = color
        self.max_size = max_size
        self.contents = []

    def put(self, item):
        """Add an item to the backpack's list of contents unless max_size is already full. In that
        case return 'No Room!'"""
        if len(self.contents) >= self.max_size:
            print("No Room!")
        else:
            self.contents.append(item)

    def take(self, item):
        """Remove an item from the backpack's list of contents."""
        self.contents.remove(item)
        
    def dump(self):
        """Reset the contents of the backpack to an empty list."""
        self.contents.clear()

    # Magic Methods -----------------------------------------------------------

    # Problem 3: Write __eq__() and __str__().
    def __add__(self, other):
        """Add the number of contents of each Backpack."""
        return len(self.contents) + len(other.contents)

    def __lt__(self, other):
        """Compare two backpacks. If 'self' has fewer contents
        than 'other', return True. Otherwise, return False.
        """
        return len(self.contents) < len(other.contents)
    
    def __eq__(self, other):
        """Determine if the two backpacks are equal if they have the same name, color,
        and amount of contents."""
        if self.name == other.name and self.color == other.color and len(self.contents) == len(other.contents):
            return True
        else:
            return False
    def __str__(self):
        """Return the string representation of the backpack object. This method is invoked by str() and 
        used by print()"""
        return "Owner:\t\t" + self.name + "\nColor:\t\t" + self.color + "\nSize:\t\t" + str(len(self.contents)) +\
        "\nMax size:\t" + str(self.max_size) + "\nContents:\t" + str(self.contents)


# An example of inheritance. You are not required to modify this class.
class Knapsack(Backpack):
    """A Knapsack object class. Inherits from the Backpack class.
    A knapsack is smaller than a backpack and can be tied closed.

    Attributes:
        name (str): the name of the knapsack's owner.
        color (str): the color of the knapsack.
        max_size (int): the maximum number of items that can fit inside.
        contents (list): the contents of the backpack.
        closed (bool): whether or not the knapsack is tied shut.
    """
    def __init__(self, name, color):
        """Use the Backpack constructor to initialize the name, color,
        and max_size attributes. A knapsack only holds 3 item by default.

        Parameters:
            name (str): the name of the knapsack's owner.
            color (str): the color of the knapsack.
            max_size (int): the maximum number of items that can fit inside.
        """
        Backpack.__init__(self, name, color, max_size=3)
        self.closed = True

    def put(self, item):
        """If the knapsack is untied, use the Backpack.put() method."""
        if self.closed:
            print("I'm closed!")
        else:
            Backpack.put(self, item)

    def take(self, item):
        """If the knapsack is untied, use the Backpack.take() method."""
        if self.closed:
            print("I'm closed!")
        else:
            Backpack.take(self, item)

    def weight(self):
        """Calculate the weight of the knapsack by counting the length of the
        string representations of each item in the contents list.
        """
        return sum(len(str(item)) for item in self.contents)


# Problem 2: Write a 'Jetpack' class that inherits from the 'Backpack' class.

class Jetpack(Backpack):
    """A Jetpack object class. Inherits from the Backpack class.
    A jetpack is a backpack that consumes fuel to enable flight.
    
    Attributes:
        name (str): the name of the jetpack's owner.
        color (str): the color of the jetpack.
        max_size (int): the maximum number of items that can fit inside.
        contents (list): the contents of the backpack.
        fuel_amount (int): the quantity of fuel in the jetpack.
    """
    def __init__(self, name, color, max_size=2, fuel_amount=10):
        """Use the Backpack constructor to initialize the name, color,
        and max_size attributes. A jetpack only holds 2 item by default.
        A jetpack only holds a value of 10 fuel by default.

        Parameters:
            name (str): the name of the jetpack's owner.
            color (str): the color of the jetpack.
            max_size (int): the maximum number of items that can fit inside.
            fuel_amount (int): the quantity of fuel in the jetpack.
        """
        Backpack.__init__(self, name, color, max_size)
        self.fuel_amount = fuel_amount
    
    def fly(self, fuel_burned):
        """Allow the user to fly at the expense of using fuel. Decrease the fuel_amount attribute
        according to the fuel_burned parameter. If the fuel_burned parameter exceeds the fuel_amount
        atrribute, do not fly and instead say 'Not enough fuel!
        """
        if fuel_burned > self.fuel_amount:
            print("Not enough fuel!")
        else:
            self.fuel_amount = self.fuel_amount - fuel_burned
    
    def dump(self):
        """Override the Backpack.dump() function. Empties the contents of the backpack portion
        of the jetpack and set the amount of fuel to 0.
        """
        Backpack.dump(self)
        self.fuel_amount = 0


# Problem 4: Write a 'ComplexNumber' class.
class ComplexNumber():
    """A ComplexNumber object class. Contains a number represented by both real and imaginary components
    
    """
    
    def __init__(self, real_num, imaginary_num=0.0):
        self.real = real_num
        self.imag = imaginary_num
    
    def conjugate(self):
        neg_imag = -self.imag
        return ComplexNumber(self.real, neg_imag)
    
    def __str__(self):
        """"Return (a+bj) for a complex number with b>=0 and (a-bj) when b<0."""
        if self.imag < 0:
            return '('+str(self.real)+str(self.imag)+'j'+')'
        elif self.imag >= 0:
            return '('+str(self.real)+"+"+str(self.imag)+'j'+')'
        else:
            return str(self.real)
    
    def __abs__(self):
        """Return the magnitude of the complex number which is sqrt(a^2+b^2)."""
        return (self.real**2+self.imag**2)**(.5)
    
    def __eq__(self,other):
        """Compare if two ComplexNumber objects are the same by comparing if they have the same real
        and imaginary parts."""
        if self.real == other.real and self.imag == other.imag:
            return True
        else:
            return False
    
    def __add__(self,other):
        """Add a complex number with another number, either real or complex"""
        return ComplexNumber(self.real+other.real,self.imag+other.imag)
      
    def __sub__(self,other):
        """Subtract a number from a complex number"""
        return ComplexNumber(self.real-other.real,self.imag-other.imag)
    
    def __mul__(self,other):
        """Multiply a complex number with another number, either real or complex"""
        return ComplexNumber((self.real*other.real)-(self.imag*other.imag),self.real*other.imag+self.imag*other.real)  
      
    def __truediv__(self,other):
        """Divide a real or complex number from a complex number"""
        denominator = (other*other.conjugate()).real
        numerator = self*other.conjugate()
        return ComplexNumber(numerator.real/denominator,numerator.imag/denominator)

def test_ComplexNumber(a, b):
    py_cnum, my_cnum = complex(a, b), ComplexNumber(a, b)
    # Validate the constructor.
    if my_cnum.real != a or my_cnum.imag != b:
        print("__init__() set self.real and self.imag incorrectly")
    # Validate conjugate() by checking the new number's imag attribute.
    if py_cnum.conjugate().imag != my_cnum.conjugate().imag:
        print("conjugate() failed for", py_cnum)
    # Validate __str__().
    if str(py_cnum) != str(my_cnum):
        print("__str__() failed for", py_cnum)
    # Validate __abs__().
    if abs(py_cnum) != abs(my_cnum):
        print("__abs__() failed for", py_cnum)
    # Validate __eq__().
    if not(py_cnum == my_cnum):
        print("__eq__() failed for", py_cnum)
    # Validate __add__().
    if not(py_cnum+py_cnum == my_cnum+my_cnum):
        print("__add__() failed for", py_cnum)
        # Validate __sub__().
    if not(py_cnum-py_cnum == my_cnum-my_cnum):
        print("__sub__() failed for", py_cnum)
    # Validate __mul__().
    if not(py_cnum*py_cnum == my_cnum*my_cnum):
        print("__mul__() failed for", py_cnum)
    # Validate __truediv__().
    if not(py_cnum/py_cnum == my_cnum/my_cnum):
        print("__truediv__() failed for", py_cnum)

def test_backpack():
    """Test function for debugging purposes."""
    
    testpack = Backpack("Barry", "black")       # Instantiate the object.
    if testpack.name != "Barry":                # Test an attribute.
        print("Backpack.name assigned incorrectly")
    for item in ["pencil", "pen", "paper", "computer"]:
        testpack.put(item)                      # Test a method.
    print(str(testpack))
