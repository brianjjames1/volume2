# nearest_neighbor.py
"""Volume 2: Nearest Neighbor Search.
<Name> Brian James
<Class>
<Date>
"""

import numpy as np
from scipy.spatial import KDTree
from scipy import stats
from matplotlib import pyplot as plt


# Problem 1
def exhaustive_search(X, z):
    """Solve the nearest neighbor search problem with an exhaustive search.

    Parameters:
        X ((m,k) ndarray): a training set of m k-dimensional points.
        z ((k, ) ndarray): a k-dimensional target point.

    Returns:
        ((k,) ndarray) the element (row) of X that is nearest to z.
        (float) The Euclidean distance from the nearest neighbor to z.
    """
    min_d = np.infty
    nearest_neighbor = []
    for x in X:
        distance = np.linalg.norm(x-z)
        if distance < min_d:
            min_d = distance
            nearest_neighbor = x
    return nearest_neighbor, min_d    


# Problem 2: Write a KDTNode class.
class KDTNode:
    """Node class for K-D Trees.

    Attributes:
        left (KDTNode): a reference to this node's left child.
        right (KDTNode): a reference to this node's right child.
        value ((k,) ndarray): a coordinate in k-dimensional space.
        pivot (int): the dimension of the value to make comparisons on.
    """
    def __init__(self,x):
        if not isinstance(x,np.ndarray):
            raise TypeError("Data must be a NumPy array")
        self.value = x
        self.left = None
        self.right = None
        self.pivot = None

# Problems 3 and 4
class KDT:
    """A k-dimensional binary tree for solving the nearest neighbor problem.

    Attributes:
        root (KDTNode): the root node of the tree. Like all other nodes in
            the tree, the root has a NumPy array of shape (k,) as its value.
        k (int): the dimension of the data in the tree.
    """
    def __init__(self):
        """Initialize the root and k attributes."""
        self.root = None
        self.k = None

    def find(self, data):
        """Return the node containing the data. If there is no such node in
        the tree, or if the tree is empty, raise a ValueError.
        """
        def _step(current):
            """Recursively step through the tree until finding the node
            containing the data. If there is no such node, raise a ValueError.
            """
            if current is None:                     # Base case 1: dead end.
                raise ValueError(str(data) + " is not in the tree")
            elif np.allclose(data, current.value):
                return current                      # Base case 2: data found!
            elif data[current.pivot] < current.value[current.pivot]:
                return _step(current.left)          # Recursively search left.
            else:
                return _step(current.right)         # Recursively search right.

        # Start the recursive search at the root of the tree.
        return _step(self.root)

    # Problem 3
    def insert(self, data):
        """Insert a new node containing the specified data.

        Parameters:
            data ((k,) ndarray): a k-dimensional point to insert into the tree.

        Raises:
            ValueError: if data does not have the same dimensions as other
                values in the tree.
            ValueError: if data is already in the tree
        """
        if self.root == None:       # create new tree if tree is empty
            new_node = KDTNode(data)
            new_node.pivot = 0
            self.k = len(data)
            self.root = new_node
            return
            
        elif len(data) != self.k:
            raise ValueError("Data to be inserted must be of length " + str(self.k))
        
        else:
            new_node = KDTNode(data)
            
            def _step(current):
                """Recursively step through the tree until finding the node that should become the parent, then insert"""
                if np.allclose(data,current.value):
                    raise ValueError("Duplicates not allowed in tree")
                elif data[current.pivot] < current.value[current.pivot]:
                    if current.left == None:
                        if current.pivot == self.k - 1:
                            new_node.pivot = 0
                        else:
                            new_node.pivot = current.pivot + 1
                        current.left = new_node
                    else:
                        _step(current.left)
                elif data[current.pivot] >= current.value[current.pivot]:
                    if current.right == None:
                        if current.pivot == self.k - 1:
                            new_node.pivot = 0
                        else:
                            new_node.pivot = current.pivot + 1
                        current.right = new_node
                    else:
                        _step(current.right)
                
            _step(self.root)

    # Problem 4
    def query(self, z):
        """Find the value in the tree that is nearest to z.

        Parameters:
            z ((k,) ndarray): a k-dimensional target point.

        Returns:
            ((k,) ndarray) the value in the tree that is nearest to z.
            (float) The Euclidean distance from the nearest neighbor to z.
        """
        def KDSearch(current,nearest,min_d):
            if current is None:
                return nearest,min_d
            x = current.value
            i = current.pivot
            dis = np.linalg.norm(x-z)
            if dis < min_d:
                nearest = current
                min_d = dis
            if z[i] < x[i]:
                nearest,min_d = KDSearch(current.left,nearest,min_d)
                if z[i]+min_d >= x[i]:
                    nearest,min_d = KDSearch(current.right,nearest,min_d)
            else:
                nearest,min_d = KDSearch(current.right,nearest,min_d)
                if z[i]-min_d <= x[i]:
                    nearest,min_d = KDSearch(current.left,nearest,min_d)
            return nearest,min_d
        node,min_d = KDSearch(self.root,self.root,np.linalg.norm(self.root.value-z))
        return node.value,min_d

    def __str__(self):
        """String representation: a hierarchical list of nodes and their axes.

        Example:                           'KDT(k=2)
                    [5,5]                   [5 5]   pivot = 0
                    /   \                   [3 2]   pivot = 1
                [3,2]   [8,4]               [8 4]   pivot = 1
                    \       \               [2 6]   pivot = 0
                    [2,6]   [7,5]           [7 5]   pivot = 0'
        """
        if self.root is None:
            return "Empty KDT"
        nodes, strs = [self.root], []
        while nodes:
            current = nodes.pop(0)
            strs.append("{}\tpivot = {}".format(current.value, current.pivot))
            for child in [current.left, current.right]:
                if child:
                    nodes.append(child)
        return "KDT(k={})\n".format(self.k) + "\n".join(strs)


# Problem 5: Write a KNeighborsClassifier class.
class KNeighborsClassifier:
    """A k-nearest neighbors classifier that uses SciPy's KDTree to solve
    the nearest neighbor problem efficiently.
    """
    def __init__(self,n_neighbors):
        self.n_neighbors = n_neighbors
    
    def fit(self,X,y):
        """
        Parameters
        ----------
        X : m x k dimensional array (training set)
        y : 1 dimensional array (training label)

        Returns
        -------
        None.
        """
        self.tree = KDTree(X)
        self.labels = y
    
    def predict(self,z):
        """
        Parameters
        ----------
        z : 1 dimensional array with k entries

        Returns
        -------
        The most common label
        """
        distances, indices = self.tree.query(z,self.n_neighbors)
        label_index = stats.mode(indices)[0][0]
        return self.labels[label_index]
        
        
        

# Problem 6
def prob6(n_neighbors, filename="mnist_subset.npz"):
    """Extract the data from the given file. Load a KNeighborsClassifier with
    the training data and the corresponding labels. Use the classifier to
    predict labels for the test data. Return the classification accuracy, the
    percentage of predictions that match the test labels.

    Parameters:
        n_neighbors (int): the number of neighbors to use for classification.
        filename (str): the name of the data file. Should be an npz file with
            keys 'X_train', 'y_train', 'X_test', and 'y_test'.

    Returns:
        (float): the classification accuracy.
    """
    
    data = np.load(filename)
    X_train = data["X_train"].astype(np.float)
    y_train = data["y_train"]
    X_test = data["X_test"].astype(np.float)
    y_test = data["y_test"]
    
    kneighbor =  KNeighborsClassifier(n_neighbors)
    kneighbor.fit(X_train,y_train)
    my_labels = []
    
    for image in X_test:
        my_labels.append(kneighbor.predict(image))
    
    """Compare the number of matched labels with y_test"""
    num_matches = 0
    size = len(my_labels)
    for i in range(size):
        if my_labels[i] == y_test[i]:
            num_matches += 1
    accuracy = num_matches / size
    return accuracy
    
    
"""
A = np.array([1,4,5])
B = np.array([1,5,6])
C = np.array([2,3,99])
obj1 = KDT()
obj1.insert(A)
obj1.insert(B)
obj1.insert(C)
"""
"""
data = np.random.random((100,5))    # 100 5-dimensional points.
target = np.random.random(5)
tree = KDTree(data)

min_distance, index = tree.query(target)
print(min_distance)
print(tree.data[index])

myKDT = KDT()
for x in data:
    myKDT.insert(x)
my_node, my_distance = myKDT.query(target)
print(my_distance)
print(my_node)
"""
#prob6(4)