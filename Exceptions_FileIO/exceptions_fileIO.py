# exceptions_fileIO.py
"""Python Essentials: Exceptions and File Input/Output.
<Name> Brian James
<Class> Sec 03
<Date>
"""

from random import choice
import numpy as np


# Problem 1
def arithmagic():
    """
    Takes in user input to perform a magic trick and prints the result.
    Verifies the user's input at each step and raises a
    ValueError with an informative error message if any of the following occur:
    
    The first number step_1 is not a 3-digit number.
    The first number's first and last digits differ by less than $2$.
    The second number step_2 is not the reverse of the first number.
    The third number step_3 is not the positive difference of the first two numbers.
    The fourth number step_4 is not the reverse of the third number.
    """
    
    step_1 = input("Enter a 3-digit number where the first and last "
                                           "digits differ by 2 or more: ")
    
    
    if len(step_1)!=3:
        raise Exception("step_1 is not a 3 digit number.")
    if abs(int(step_1[0])-int(step_1[2]))<2:
        raise Exception("step_1's first and last digits differ by less than 2.")
        
    step_2 = input("Enter the reverse of the first number, obtained "
                                              "by reading it backwards: ")
    if step_2[::-1] != step_1:
        raise Exception("step_2 is not the reverse of step_1.")
        
    step_3 = input("Enter the positive difference of these numbers: ")
    if abs(int(step_1)-int(step_2)) != int(step_3):
        raise Exception("step_3 is not the positive difference between step_1 and step_2.")
    
    step_4 = input("Enter the reverse of the previous result: ")
    if step_4[::-1] != step_3:
        raise Exception("step_4 is not the reverse of step_3.")
    print(str(step_3), "+", str(step_4), "= 1089 (ta-da!)")


# Problem 2
def random_walk(max_iters=1e12):
    """
    If the user raises a KeyboardInterrupt by pressing ctrl+c while the 
    program is running, the function should catch the exception and 
    print "Process interrupted at iteration $i$".
    If no KeyboardInterrupt is raised, print "Process completed".

    Return walk.
    """
    try:
        walk = 0
        directions = [1, -1]
        i=0
        for i in range(int(max_iters)):
            walk += choice(directions)
    except KeyboardInterrupt:
        print("Process interrupted at iteration " + str(i))
    else:
        print("Process completed")
    finally:
        return walk
        


# Problems 3 and 4: Write a 'ContentFilter' class.
    """Class for reading in file
        
    Attributes:
        filename (str): The name of the file
        contents (str): the contents of the file
        num_letter (int): the number of alphabetic characters in the file
        num_digit (int): the number of numeric characters in the file
        num_whitespace (int): the number of whitespace characters in the file
        num_char (int): the total number of characters in the file
        num_lines (int): the number of lines in the file
        
    """
class ContentFilter(object):   
    # Problem 3
    def __init__(self, filename):
        """Read from the specified file. If the filename is invalid, prompt
        the user until a valid filename is given.
        """
        while True:
            try:
                with open(filename, 'r') as self.filename:
                    """Initialize the various attributes of the text file """
                    self.contents = self.filename.readlines()
                    self.num_letter = 0
                    self.num_digit = 0
                    self.num_whitespace = 0
                    self.num_char = 0
                    self.num_lines = 0
                    
                    #Iterate through all the characters in the file
                    for line in self.contents:
                        for i in range(len(line)):
                            if line[i].isalpha():
                                self.num_letter = self.num_letter + 1
                            if line[i].isdigit():
                                self.num_digit = self.num_digit + 1
                            if line[i].isspace():
                                self.num_whitespace = self.num_whitespace + 1
                            self.num_char += 1
                    
                    self.num_lines = len(self.contents)
                    
                break;
            except FileNotFoundError:
                filename = input("Please enter a valid file name: ")
                
    def eval_stats(self):    
        """ Reevaluate the stats of the text file. It may have been modified. """
        self.num_letter = 0
        self.num_digit = 0
        self.num_whitespace = 0
        self.num_char = 0
        self.num_lines = 0
        
        #Iterate through all the characters in the file
        for line in self.contents:
            for i in range(len(line)):
                if line[i].isalpha():
                    self.num_letter = self.num_letter + 1
                if line[i].isdigit():
                    self.num_digit = self.num_digit + 1
                if line[i].isspace():
                    self.num_whitespace = self.num_whitespace + 1
                self.num_char += 1
        
        self.num_lines = len(self.contents)
            
    
 # Problem 4 ---------------------------------------------------------------
    def check_mode(self, mode):
        """Raise a ValueError if the mode is invalid."""
        if mode != 'w' or 'x' or 'a':
            raise ValueError("Invalid mode, mode must be either 'w', 'x', or 'a'")

    def uniform(self, outfile, mode='w', case='upper'):
        """Write the data to the outfile in uniform case."""
        with open(outfile, mode) as output:
            if case=='upper':
                output.write(self.contents.upper())
            elif case=='lower':
                output.write(self.contents.lower())
            else:
                raise ValueError("case must be either 'upper' or 'lower'")


    def reverse(self, outfile, mode='w', unit='word'):
        """Write the data to the outfile in reverse order."""
        with open(outfile, mode) as output:
            if unit=='word':
                line_list = self.contents.split('\n')
                mod_contents = [words.split()[::-1] for words in line_list]
                output.write(mod_contents)
            if unit=='line':
                line_list = self.contents.split('\n')
                mod_contents = line_list[::-1]
                output.write(mod_contents)
            else:
                raise ValueError("unit must be either 'word' or 'line'")

    def transpose(self, outfile, mode='w'):
        """Write the transposed version of the data to the outfile."""
        with open(outfile,mode) as output:
            matrix = []
            word_list = [matrix.append(line.split()) for line in self.contents]
            transposed_matrix = np.transpose(matrix)
            
            newline = ''
            for line in transposed_matrix:
                output.write(newline + ' '.join(line))
                newline = '\n'

    def __str__(self):
        """String representation: info about the contents of the file."""
        #Update the data of the file
        self.eval_stats()
        #Create the output string by appending the lines to it"
        outputstring = "" 
        outputstring += "Source file:\t\t\t" + str(self.filename.name) + '\n'
        outputstring += "Total characters:\t\t" + str(self.num_char) + '\n'
        outputstring += "Alphabetic characters:\t" + str(self.num_letter) + '\n'
        outputstring += "Numerical characters:\t\t" + str(self.num_digit) + '\n'
        outputstring += "Whitespace characters:\t" + str(self.num_whitespace) + '\n'
        outputstring += "Number of lines:\t\t\t" + str(self.num_lines) + '\n'
        
        return outputstring