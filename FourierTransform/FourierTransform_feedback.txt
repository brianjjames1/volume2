12/04/21 20:42

fourier_transform.ipynb has not been modified yet

-------------------------------------------------------------------------------

12/07/21 11:28

fourier_transform.ipynb has not been modified yet

-------------------------------------------------------------------------------

12/09/21 11:27

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (5 points):
Score += 5

Problem 4 (5 points):
Score += 5

Problem 5 (5 points):
Score += 5

Problem 6 (10 points):
Score += 10

Problem 7 (5 points):
Score += 5

Problem 8 (5 points):
There are 4 notes, the last one is D.
Score += 4

Code Quality (5 points):
Score += 5

Total score: 49/50 = 98.0%

Excellent!

-------------------------------------------------------------------------------

01/13/22 13:03

Problem 1 (5 points):
Score += 5

Problem 2 (10 points):
Your convolution doesn't sound right, it sounds like it plays forward and then backwards. The volume should go up and down like a sin wave
Score += 7

Problem 3 (5 points):
'chopin_balloon.wav' doesn't start on the right note. It should start on the same note chopin.wav starts on. It starts on the last note.
Score += 4

Problem 4 (10 points):
clean_noisy2.wav sounds like you are missing cleaning some frequencies. There's still that high pitched sound overpowering the voice. Plot the dft again to make sure you got all of them!
Score += 8

Problem 5 (5 points):
Score += 5

Problem 6 (10 points):
Score += 10

Code Quality (5 points):
Score += 5

Total score: 44/50 = 86.0%

-------------------------------------------------------------------------------

