#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 18:27:20 2021

@author: James
"""

from matplotlib import pyplot as plt
from scipy.io import wavfile
from scipy.fftpack import fft as fast
import numpy as np
import IPython

plt.rcParams["figure.dpi"] = 300             # Fix plot quality.
plt.rcParams["figure.figsize"] = (12,3)      # Change plot size / aspect (you may adjust this).

class SoundWave(object):
    """A class for working with digital audio signals."""

    # Problem 1.1
    def __init__(self, rate, samples):
        """Set the SoundWave class attributes.

        Parameters:
            rate (int): The sample rate of the sound.
            samples ((n,) ndarray): NumPy array of samples.
        """
        self.rate = rate
        self.samples = samples

    # Problems 1.1 and 1.7
    def plot(self,freq=False):
        """Plot the graph of the sound wave (time versus amplitude)."""
        
        if freq:
            f_hat = fast(self.samples)
            n = len(f_hat)
            hertz = []
            for k in range(len(f_hat)):
                hertz.append(k*self.rate/len(self.samples))
            hertz = hertz[:n//2]
            f_hat = np.abs(f_hat)[:n//2]
            plt.plot(hertz,f_hat)
            plt.show()
            
        else:
            time = len(self.samples) / self.rate
            x_points = np.linspace(0,time,len(self.samples))
            plt.plot(x_points,self.samples)
            plt.ylim([-32768,32767])
            plt.xlabel("Time (seconds)")
            plt.ylabel("Samples")
            plt.show()
        
    # Problem 1.2
    def export(self, filename, force=False):
        """Generate a wav file from the sample rate and samples. 
        If the array of samples is not of type np.int16, scale it before exporting.

        Parameters:
            filename (str): The name of the wav file to export the sound to.
        """
        scaled = self.samples
        if not isinstance(self.samples[0],np.int16) or force: #Scale samples if not of type np.int16
            scaled = np.int16(((self.samples)/np.abs(self.samples).max())*32767)
            
        wavfile.write(filename,self.rate,scaled)
    
    # Problem 1.4
    def __add__(self, other):
        """Combine the samples from two SoundWave objects.

        Parameters:
            other (SoundWave): An object containing the samples to add
                to the samples contained in this object.
        
        Returns:
            (SoundWave): A new SoundWave instance with the combined samples.

        Raises:
            ValueError: if the two sample arrays are not the same length.
        """
        if len(self.samples) != len(other.samples):
            raise ValueError("Samples must be of same length")
        else:
            return SoundWave(self.rate,self.samples+other.samples)

    # Problem 1.4
    def __rshift__(self, other):
        """Concatentate the samples from two SoundWave objects.

        Parameters:
            other (SoundWave): An object containing the samples to concatenate
                to the samples contained in this object.

        Raises:
            ValueError: if the two sample rates are not equal.
        """
        if self.rate != other.rate:
            raise ValueError("Sample rates must be equal")
        else:
            return SoundWave(self.rate,np.append(self.samples,other.samples))
    
    # Problem 2.1
    def __mul__(self, other):
        """Convolve the samples from two SoundWave objects using circular convolution.
        
        Parameters:
            other (SoundWave): An object containing the samples to convolve
                with the samples contained in this object.
        
        Returns:
            (SoundWave): A new SoundWave instance with the convolved samples.

        Raises:
            ValueError: if the two sample rates are not equal.
        """
        raise NotImplementedError("Problem 2.1 Incomplete")

    # Problem 2.2
    def __pow__(self, other):
        """Convolve the samples from two SoundWave objects using linear convolution.
        
        Parameters:
            other (SoundWave): An object containing the samples to convolve
                with the samples contained in this object.
        
        Returns:
            (SoundWave): A new SoundWave instance with the convolved samples.

        Raises:
            ValueError: if the two sample rates are not equal.
        """
        raise NotImplementedError("Problem 2.2 Incomplete")

    # Problem 2.4
    def clean(self, low_freq, high_freq):
        """Remove a range of frequencies from the samples using the DFT. 

        Parameters:
            low_freq (float): Lower bound of the frequency range to zero out.
            high_freq (float): Higher boound of the frequency range to zero out.
        """
        raise NotImplementedError("Problem 2.4 Incomplete")



def simple_dft(samples):
    """Compute the DFT of an array of samples.

    Parameters:
        samples ((n,) ndarray): an array of samples.
    
    Returns:
        ((n,) ndarray): The DFT of the given array.
    """
    n = len(samples)
    w_n = np.exp(2j*np.pi/n)
    W_n = np.ones(n)
    for i in range(1,n):
        row = np.array([w_n**(-i*j) for j in range(n)])
        W_n = np.vstack((W_n,row))
    F_n = 1/n * W_n
    return np.dot(F_n,samples)

def simple_fft(samples, threshold=1):
    """Compute the DFT using the FFT algorithm.
    
    Parameters:
        samples ((n,) ndarray): an array of samples.
        threshold (int): when a subarray of samples has fewer
            elements than this integer, use simple_dft() to
            compute the DFT of that subarray.
    
    Returns:
        ((n,) ndarray): The DFT of the given array.
    """
    
    def split(g):
        n = len(g)
        if n <= threshold:
            return simple_dft(g)
        else:
            even = split(g[::2])
            odd = split(g[1::2])
            #z = np.zeros(n)
            z = np.array([np.exp(-2j*np.pi*k/n) for k in range(n)])
            m = n // 2
            return np.concatenate((even + np.multiply(z[:m],odd), even + np.multiply(z[m:],odd)))
    return split(samples) / len(samples)

    
rate, samples = wavfile.read("tada.wav")
soundwave = SoundWave(rate,samples)
soundwave.plot(True)