# quassian_quadrature.py
"""Volume 2: Gaussian Quadrature.
<Name> Brian James
<Class>
<Date>
"""

from scipy import linalg as la
import numpy as np
from scipy.integrate import quad
from scipy.stats import norm
from matplotlib import pyplot as plt

class GaussianQuadrature:
    """Class for integrating functions on arbitrary intervals using Gaussian
    quadrature with the Legendre polynomials or the Chebyshev polynomials.
    """
    # Problems 1 and 3
    def __init__(self, n, polytype="legendre"):
        """Calculate and store the n points and weights corresponding to the
        specified class of orthogonal polynomial (Problem 3). Also store the
        inverse weight function w(x)^{-1} = 1 / w(x).

        Parameters:
            n (int): Number of points and weights to use in the quadrature.
            polytype (string): The class of orthogonal polynomials to use in
                the quadrature. Must be either 'legendre' or 'chebyshev'.

        Raises:
            ValueError: if polytype is not 'legendre' or 'chebyshev'.
            
        Attributes:
            points: The sampling points for the quadrature
            weights = The weights corresponding to the points
            w_recip = The recipricol of the weight function
        """
        
        if polytype != "legendre" and polytype != "chebyshev":
            raise ValueError("second arg must be 'legendre' or 'chebyshev'")
        else:
            self.n = n
            self.polytype = polytype
            if polytype == "legendre":
                self.w_recip = lambda x : 1
            elif polytype == "chebyshev":
                self.w_recip = lambda x : np.sqrt(1-x**2)
            self.points, self.weights = self.points_weights(n)

    # Problem 2
    def points_weights(self, n):
        """Calculate the n points and weights for Gaussian quadrature.

        Parameters:
            n (int): The number of desired points and weights.

        Returns:
            points ((n,) ndarray): The sampling points for the quadrature.
            weights ((n,) ndarray): The weights corresponding to the points.
        """
        
        if self.polytype == "legendre":     # create Jacobi matrix based on legendre alpha and beta
            beta_f = lambda k: k**2/(4*k**2-1)
            beta = np.sqrt(np.array([beta_f(k) for k in range(1,n)]))
            jacobi_m = np.diag(beta,k=1) + np.diag(beta,k=-1)
            u_w = 2             # measure of the weight function
        elif self.polytype == "chebyshev":
            beta = np.sqrt(np.array([1/2 if k == 1 else 1/4 for k in range(1,n)]))
            jacobi_m = np.diag(beta,k=1) + np.diag(beta,k=-1)
            u_w = np.pi         # measure of the weight function
            
        # Compute eigenvalues and eigenvectors of jacobi_m
        eig_val, eig_vec = la.eig(jacobi_m)
        x_points = eig_val.real
        w = []
        
        for i in range(n):
            w_i = u_w * (eig_vec[0,i])**2        # measure of the weight function * first ele of ith eigenvector
            w.append(w_i)
        
        return x_points, np.array(w)
        

    # Problem 3
    def basic(self, f):
        """Approximate the integral of a f on the interval [-1,1]."""
        g = f(self.points) * self.w_recip(self.points)
        return np.inner(self.weights,g)   # inner product

    # Problem 4
    def integrate(self, f, a, b):
        """Approximate the integral of a function on the interval [a,b].

        Parameters:
            f (function): Callable function to integrate.
            a (float): Lower bound of integration.
            b (float): Upper bound of integration.

        Returns:
            (float): Approximate value of the integral.
        """
        # Define function h
        h = lambda x: f((b-a)/2*x+(a+b)/2)
        # Approximate value of the integral
        return (b-a)/2 * self.basic(h)

    # Problem 6.
    def integrate2d(self, f, a1, b1, a2, b2):
        """Approximate the integral of the two-dimensional function f on
        the interval [a1,b1]x[a2,b2].

        Parameters:
            f (function): A function to integrate that takes two parameters.
            a1 (float): Lower bound of integration in the x-dimension.
            b1 (float): Upper bound of integration in the x-dimension.
            a2 (float): Lower bound of integration in the y-dimension.
            b2 (float): Upper bound of integration in the y-dimension.

        Returns:
            (float): Approximate value of the integral.
        """
        
        x_pts = (b1-a1)*self.points/2+(a1+b1)/2
        y_pts = (b2-a2)*self.points/2+(a2+b2)/2
        
        # Create an nxn matrix for the g function
        for i in range(self.n):
            row_array = []  
            for j in range(self.n):
                row_array.append(f(x_pts[i],y_pts[j])*self.w_recip(x_pts[i])*self.w_recip(y_pts[j]))
            row_array = np.array(row_array)
            if i == 0:
                g = row_array
            else:
                g = np.vstack((g,row_array))

        # Double sum of w_i*w_j*g(z_i,z_j)
        total = 0
        for i in range(self.n):
            for j in range(self.n):
                total += self.weights[i]*self.weights[j]*g[i,j]
        
        return (b1-a1)*(b2-a2)/4 * total  # Approximate value of the integral


# Problem 5
def prob5():
    """Use scipy.stats to calculate the "exact" value F of the integral of
    f(x) = (1/sqrt(2 pi))e^((-x^2)/2) from -3 to 2. Then repeat the following
    experiment for n = 5, 10, 15, ..., 50.
        1. Use the GaussianQuadrature class with the Legendre polynomials to
           approximate F using n points and weights. Calculate and record the
           error of the approximation.
        2. Use the GaussianQuadrature class with the Chebyshev polynomials to
           approximate F using n points and weights. Calculate and record the
           error of the approximation.
    Plot the errors against the number of points and weights n, using a log
    scale for the y-axis. Finally, plot a horizontal line showing the error of
    scipy.integrate.quad() (which doesn’t depend on n).
    """
    
    exact_I = norm.cdf(2) - norm.cdf(-3)
    f = lambda x : (1/np.sqrt(2*np.pi))*np.exp((-x**2)/2)
    legendre_error = []
    chebyshev_error = []
    N = 5*np.arange(1,11)
    
    for n in N:
        # Create the GaussianQuadrature objects
        legendre_obj = GaussianQuadrature(n,"legendre")
        chebyshev_obj = GaussianQuadrature(n,"chebyshev")
        
        #Calculate the integral of each method
        legendre_I = legendre_obj.integrate(f,-3,2)
        chebyshev_I = chebyshev_obj.integrate(f,-3,2)
        
        # Calculate the error of each method
        legendre_error.append(np.abs(exact_I - legendre_I))
        chebyshev_error.append(np.abs(exact_I-chebyshev_I))
        
    # Plot with log scale y axis
    plt.semilogy(N,legendre_error,label='legendre integral error')
    plt.semilogy(N,chebyshev_error,label='chebyshev integral error')

    scipy_error = np.abs(exact_I-quad(f,-3,2)[0])*np.ones_like(N)
    
    plt.semilogy(N,scipy_error,label='scipy.integrate.quad error')
    plt.legend()
    plt.xlabel("n number of points")
    plt.ylabel("error from the exact integral")
    plt.title("Problem 5 Plot")
    plt.show()