# test_specs.py
"""Python Essentials: Unit Testing.
<Name>
<Class>
<Date>
"""

import specs
import pytest


def test_add():
    assert specs.add(1, 3) == 4, "failed on positive integers"
    assert specs.add(-5, -7) == -12, "failed on negative integers"
    assert specs.add(-6, 14) == 8

def test_divide():
    assert specs.divide(4,2) == 2, "integer division"
    assert specs.divide(5,4) == 1.25, "float division"
    with pytest.raises(ZeroDivisionError) as excinfo:
        specs.divide(4, 0)
    assert excinfo.value.args[0] == "second input cannot be zero"


# Problem 1: write a unit test for specs.smallest_factor(), then correct it.
def test_smallest_factor():
    assert specs.smallest_factor(4)==2, "failed on positive integer"
    assert specs.smallest_factor(7)==7, "failed on prime number"


# Problem 2: write a unit test for specs.month_length().
def test_month_length():
    assert specs.month_length("January")==31, "failed on 31 day month"
    assert specs.month_length("February")==28, "failed on February on a non leap year"
    assert specs.month_length("February",True)==29 ,"failed on February for leap year"
    assert specs.month_length("April")==30, "failed on a 30 day month"
    assert specs.month_length("Thirteentember")==None, "failed to return nothing for an incorrect month"

# Problem 3: write a unit test for specs.operate().
def test_operate():
    with pytest.raises(TypeError) as excinfo:
        specs.operate(1, 2, 5)
    assert excinfo.value.args[0] == "oper must be a string", "failed to raise a TypeError exception"
    with pytest.raises(ZeroDivisionError) as excinfo:
        specs.operate(4,0,'/')
    assert excinfo.value.args[0] == "division by zero is undefined", "failed to raise a ZeroDivisionError exception"
    with pytest.raises(ValueError) as excinfo:
        specs.operate(5,3,'%')
    assert excinfo.value.args[0] == "oper must be one of '+', '/', '-', or '*'", "failed to throw on invalid operator input"
    assert specs.operate(2,2,'+')==4, "failed an addition operation"
    assert specs.operate(3,3,'-')==0, "failed a subtraction operation"
    assert specs.operate(4,5,'*')==20, "failed a multiplication operation"
    assert specs.operate(9,3,'/')==3, "failed a division operation"


# Problem 4: write unit tests for specs.Fraction, then correct it.
@pytest.fixture
def set_up_fractions():
    frac_1_3 = specs.Fraction(1, 3)
    frac_1_2 = specs.Fraction(1, 2)
    frac_n2_3 = specs.Fraction(-2, 3)
    return frac_1_3, frac_1_2, frac_n2_3

def test_fraction_init(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert frac_1_3.numer == 1
    assert frac_1_2.denom == 2
    assert frac_n2_3.numer == -2
    frac = specs.Fraction(30, 42)
    assert frac.numer == 5
    assert frac.denom == 7
    with pytest.raises(ZeroDivisionError) as excinfo:
        specs.Fraction(4,0)
    assert excinfo.value.args[0] == "denominator cannot be zero"
    with pytest.raises(TypeError) as excinfo:
        specs.Fraction("Hello World",1)
    assert excinfo.value.args[0] == "numerator and denominator must be integers"

def test_fraction_str(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert str(frac_1_3) == "1/3"
    assert str(frac_1_2) == "1/2"
    assert str(frac_n2_3) == "-2/3"
    assert str(specs.Fraction(5, 1)) == "5"

def test_fraction_float(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert float(frac_1_3) == 1 / 3.
    assert float(frac_1_2) == .5
    assert float(frac_n2_3) == -2 / 3.

def test_fraction_eq(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert frac_1_2 == specs.Fraction(1, 2)
    assert frac_1_3 == specs.Fraction(2, 6)
    assert frac_n2_3 == specs.Fraction(8, -12)
    assert frac_1_2 == 1 / 2

def test_fraction_add(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert (frac_1_3 + frac_1_2) == 5 / 6

def test_fraction_sub(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert (frac_1_2 - frac_1_3) == 1 / 6

def test_fraction_mul(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert (frac_1_3 * frac_1_2) == 1 / 6
    
def test_fraction_truediv(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    with pytest.raises(ZeroDivisionError) as excinfo:
        frac_1_3 / specs.Fraction(0,5)
    assert excinfo.value.args[0] == "cannot divide by zero"
    assert (frac_1_3 / frac_1_2) == 2 / 3


# Problem 5: Write test cases for Set.
def test_count_sets():
    hand1 = ["1022", "1122", "0100", "2021",
         "0010", "2201", "2111", "0020",
         "1102", "0210", "2110", "1020"]
    hand2 = ["1022", "1022", "0100", "2021",
         "0010", "2201", "2111", "0020",
         "1102", "0210", "2110", "1020"]
    hand3 = ["10220", "1122", "0100", "2021",
         "0010", "2201", "2111", "0020",
         "1102", "0210", "2110", "1020"]
    hand4 = ["3022", "1122", "0100", "2021",
      "0010", "2201", "2111", "0020",
      "1102", "0210", "2110", "1020"]
    assert specs.count_sets(hand1)==6, "failed to compute the correct amount of sets"
    with pytest.raises(ValueError) as excinfo:
        specs.count_sets(["1000"])
    assert excinfo.value.args[0] == "List must contain exactly 12 cards"
    with pytest.raises(ValueError) as excinfo:
        specs.count_sets(hand2)
    assert excinfo.value.args[0] == "All cards must be unique"
    with pytest.raises(ValueError) as excinfo:
        specs.count_sets(hand3)
    assert excinfo.value.args[0] == "Cards must have exactly 4 digits"
    with pytest.raises(ValueError) as excinfo:
        specs.count_sets(hand4)
    assert excinfo.value.args[0] == "Cards must only have the characters '0','1', or '2'"

def test_is_set():
    assert specs.is_set("1022","1031","1013") == True
    assert specs.is_set("1022","1031","1033") == False
    
