# cvxpy_intro.py
"""Volume 2: Intro to CVXPY.
<Name> Brian James
<Class>
<Date>
"""

import cvxpy as cp
import numpy as np

def prob1():
    """Solve the following convex optimization problem:

    minimize        2x + y + 3z
    subject to      x  + 2y         <= 3
                         y   - 4z   <= 1
                    2x + 10y + 3z   >= 12
                    x               >= 0
                          y         >= 0
                                z   >= 0

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """
    
    # Initialize the objective
    x = cp.Variable(3,nonneg=True)
    c = np.array([2,1,3])
    objective = cp.Minimize(c.T @ x)
    
    # Write the constraints
    G = np.array([[1,2,0],[0,1,-4]])
    P = np.array([[2,10,3],[1,0,0],[0,1,0],[0,0,1]])
    constraints = [G @ x <= [3,1], P @ x >= [12,0,0,0]]
    
    problem = cp.Problem(objective, constraints)
    optimumVal = problem.solve()
    return x.value, optimumVal


# Problem 2
def l1Min(A, b):
    """Calculate the solution to the optimization problem

        minimize    ||x||_1
        subject to  Ax = b

    Parameters:
        A ((m,n) ndarray)
        b ((m, ) ndarray)

    Returns:
        The optimizer x (ndarray)
        The optimal value (float)
    """
    
    # Initialize the objective
    n = len(A[0])
    x = cp.Variable(n,nonneg=True)
    objective = cp.Minimize(cp.norm(x,1))
    
    # Write the constraints
    constraints = [A @ x == b]
    
    problem = cp.Problem(objective, constraints)
    optimumVal = problem.solve()
    return x.value, optimumVal


# Problem 3
def prob3():
    """Solve the transportation problem by converting the last equality constraint
    into inequality constraints.

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """
    
    # Initialize the objective
    p = cp.Variable(6,nonneg=True)
    c = np.array([4,7,6,8,8,9])
    objective = cp.Minimize(c.T @ p)
    
    # Write the constraints
    G = np.array([[1,1,0,0,0,0],[0,0,1,1,0,0],[0,0,0,0,1,1]])
    P = np.array([[1,0,1,0,1,0],[0,1,0,1,0,1]])
    constraints = [G @ p <= [7,2,4], P @ p >= [5,8]]
    
    problem = cp.Problem(objective, constraints)
    optimumVal = problem.solve()
    
    return p.value, optimumVal


# Problem 4
def prob4():
    """Find the minimizer and minimum of

    g(x,y,z) = (3/2)x^2 + 2xy + xz + 2y^2 + 2yz + (3/2)z^2 + 3x + z

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """
    
    Q = np.array([[3,2,1],[2,4,2],[1,2,3]])
    r = np.array([3,0,1])
    x = cp.Variable(3)
    prob = cp.Problem(cp.Minimize(.5 * cp.quad_form(x,Q) + r.T @ x))
    minimum = prob.solve()
    
    return x.value, minimum


# Problem 5
def prob5(A, b):
    """Calculate the solution to the optimization problem
        minimize    ||Ax - b||_2
        subject to  ||x||_1 == 1
                    x >= 0
    Parameters:
        A ((m,n), ndarray)
        b ((m,), ndarray)
        
    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """
    
    # Initialize the objective
    n = len(A[0])
    x = cp.Variable(n)
    objective = cp.Minimize(cp.norm(A@x-b,2))
    
    # Create the constraints
    H = np.ones(n)
    P = np.eye(n)
    
    constraints = [H @ x == 1, P @ x >= 0]
    
    problem = cp.Problem(objective, constraints)
    minimum = problem.solve()
    
    return x.value, minimum


# Problem 6
def prob6():
    """Solve the college student food problem. Read the data in the file 
    food.npy to create a convex optimization problem. The first column is 
    the price, second is the number of servings, and the rest contain
    nutritional information. Use cvxpy to find the minimizer and primal 
    objective.
    
    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """	 
    
    # Read in the food data
    data = np.load("food.npy", allow_pickle=True)
    price = data[:,0]
    servings = data[:,1]
    calories = data[:,2]
    fat = data[:,3]
    sugar = data[:,4]
    calcium = data[:,5]
    fiber = data[:,6]
    protein = data[:,7]
    
    # Initialize the objective
    n = len(price)
    x = cp.Variable(n)
    objective = cp.Minimize(price @ x)
    
    # Create the constraints. Also multiply each nutritional information by the number of servings
    c = servings * calories
    f = servings * fat
    sHat = servings * sugar
    cHat = servings * calcium
    fHat = servings * fiber
    pHat = servings * protein
    I = np.eye(n)
    
    constraints = [c @ x <= 2000, f @ x <= 65, sHat @ x <= 50, cHat @ x >= 1000, fHat @ x >= 25, pHat @ x >= 46, I @ x >= 0]
    problem = cp.Problem(objective, constraints)
    minimum = problem.solve()
    
    return x.value, minimum

""" Response to Problem 6 

    The food you should eat the most of is potatoes. The three foods you should eat the most each week is:
        potatoes, milk, and cheese.

"""