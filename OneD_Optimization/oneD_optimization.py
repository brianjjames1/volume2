# oneD_optimization.py
"""Volume 2: One-Dimensional Optimization.
<Name> Brian James
<Class>
<Date>
"""

import numpy as np
from scipy import optimize
from matplotlib import pyplot as plt
from scipy.optimize import linesearch
from autograd import numpy as anp
from autograd import grad

# Problem 1
def golden_section(f, a, b, tol=1e-5, maxiter=15):
    """Use the golden section search to minimize the unimodal function f.

    Parameters:
        f (function): A unimodal, scalar-valued function on [a,b].
        a (float): Left bound of the domain.
        b (float): Right bound of the domain.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): The approximate minimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    converge = False
    num_iter = 0
    
    x0 = (a+b)/2
    y = (1+np.sqrt(5))/2
    for i in range(maxiter):    # iterate only maxiter times at most.
        num_iter += 1       # increaes the number of iterations looped through.
        
        c = (b-a)/y
        a1 = b-c
        b1 = a+c
        if f(a1) <= f(b1):      # get new boundaries for
            b = b1
        else:
            a = a1
        x1 = (a+b)/2    # Set the minimizer approximation at the interval midpoint.
        if np.abs(x0-x1) < tol:
            converge = True
            break    # stop iterating if the approximation converges enough
        x0 = x1
    return x1, converge, num_iter


# Problem 2
def newton1d(df, d2f, x0, tol=1e-5, maxiter=15):
    """Use Newton's method to minimize a function f:R->R.

    Parameters:
        df (function): The first derivative of f.
        d2f (function): The second derivative of f.
        x0 (float): An initial guess for the minimizer of f.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): The approximate minimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    converge = False
    num_iter = 0
    
    for i in range(maxiter):        # loop until estimate converges or up to maxiter times
        num_iter += 1
        x1 = x0 - df(x0)/d2f(x0)
        
        if np.abs(x0-x1) < tol:     # guesses have converged
            converge = True
            break
        x0 = x1
        
    return x1, converge, num_iter

# Problem 3
def secant1d(df, x0, x1, tol=1e-5, maxiter=15):
    """Use the secant method to minimize a function f:R->R.

    Parameters:
        df (function): The first derivative of f.
        x0 (float): An initial guess for the minimizer of f.
        x1 (float): Another guess for the minimizer of f.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): The approximate minimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    converge = False
    num_iter = 0
    
    for i in range(maxiter):            # loop until points converge or maxiter times
        num_iter += 1
        dfx1 = df(x1)
        dfx0 = df(x0)
        
        x2 = x1 - ((x1-x0)/(dfx1-dfx0))*dfx1
        if np.abs(x1-x2) < tol:         # if points converge
            converge = True 
            break
        x0 = x1                 # adjust points
        x1 = x2                 # adjust points
    
    return x2, converge, num_iter

# Problem 4
def backtracking(f, Df, x, p, alpha=1, rho=.9, c=1e-4):
    """Implement the backtracking line search to find a step size that
    satisfies the Armijo condition.

    Parameters:
        f (function): A function f:R^n->R.
        Df (function): The first derivative (gradient) of f.
        x (float): The current approximation to the minimizer.
        p (float): The current search direction.
        alpha (float): A large initial step length.
        rho (float): Parameter in (0, 1).
        c (float): Parameter in (0, 1).

    Returns:
        alpha (float): Optimal step size.
    """
    
    Dfp = np.dot(Df(x),p)       # Compute only once
    fx = f(x)
    while (f(x+alpha*p)) > (fx + c*alpha*Dfp):
        alpha = rho * alpha
    return alpha