"""Volume 2: Simplex

<Name> Brian James
<Date> March 3, 2022
<Class>
"""

import numpy as np


# Problems 1-6
class SimplexSolver(object):
    """Class for solving the standard linear optimization problem

                        minimize        c^Tx
                        subject to      Ax <= b
                                         x >= 0
    via the Simplex algorithm.
    """
    # Problem 1
    def __init__(self, c, A, b):
        """Check for feasibility and initialize the dictionary.

        Parameters:
            c ((n,) ndarray): The coefficients of the objective function.
            A ((m,n) ndarray): The constraint coefficients matrix.
            b ((m,) ndarray): The constraint vector.

        Raises:
            ValueError: if the given system is infeasible at the origin.
        """
        
        n = len(b)
        origin = np.zeros(n)
        if all(origin<=b):
            self.c = c
            self.A = A
            self.b = b
        else:
            raise ValueError("the given system is infeasible at the origin")

    # Problem 2
    def _generatedictionary(self, c, A, b):
        """Generate the initial dictionary.

        Parameters:
            c ((n,) ndarray): The coefficients of the objective function.
            A ((m,n) ndarray): The constraint coefficients matrix.
            b ((m,) ndarray): The constraint vector.
        """
        m = len(A)
        I = np.eye(m)
        Abar = np.hstack((A,I))             # the coefficient matrix with slack variables added
        
        zeros = np.zeros(m)
        cbar = np.concatenate((c,zeros),axis=None)
        
        r1 = np.concatenate((np.array([0]),cbar))           # the first row of the dictionary matrix
        r2 = np.hstack((b.reshape((m,1)),-Abar))            # the second row the dictionary matrix
        self.D = np.vstack((r1,r2))


    # Problem 3a
    def _pivot_col(self):
        """Return the column index of the next pivot column.
        """
        enteringIndex = None
        for i in range(1,len(self.D[0][1:])):
            if self.D[0:,i][0] < 0:
                enteringIndex = i
                return enteringIndex
        if enteringIndex == None:
            return None

    # Problem 3b
    def _pivot_row(self, index):
        """Determine the row index of the next pivot row using the ratio test
        (Bland's Rule).
        """
        
        # Check and see if all the entries in the column are positive. If so, throw exception. There is no solution.
        if np.all(self.D[:,index] >= 0):
            raise ValueError("No solution. The problem is unbounded.")
        
        ratios = [-i[0]/i[index] if i[index] < 0 else np.inf for i in self.D[1:]]
        # find the smallest binding ratio. (Must be >= 0)
        
        leavingIndex = np.argmin(ratios)+1              # offset by 1 to account for indexing error
        
        return leavingIndex

    # Problem 4
    def pivot(self):
        """Select the column and row to pivot on. Reduce the column to a
        negative elementary vector.
        """
        
        pivotCol = self._pivot_col()
        pivotRow = self._pivot_row(pivotCol)
        
        self.D[pivotRow] = self.D[pivotRow] / -self.D[pivotRow,pivotCol]
        
        # zero out the entries in the columns above and below the pivot entry with elementary row operations
        for i in range(len(self.D)):
            if i != pivotRow:         # if the row isn't the same as the pivot row, zero the corresponding column entry
                zeroingFactor = self.D[i,pivotCol]
                self.D[i] = self.D[i] + zeroingFactor * self.D[pivotRow]

    # Problem 5
    def solve(self):
        """Solve the linear optimization problem.

        Returns:
            (float) The minimum value of the objective function.
            (dict): The basic variables and their values.
            (dict): The nonbasic variables and their values.
        """
        
        self._generatedictionary(self.c, self.A, self.b)
        
        while (np.any(self.D[0][1:] < 0)):              # if any entry in the function row is negative (except the first entry, continue to pivot
            self.pivot()
            
        # Create a dictionary. (Min, Dependent Dictionary, Independent Dictionary)
        valColumn = self.D[:,0]
        funcVar = self.D[0,:][1:]
        dependDict = {}
        independDict = {}
        for col in range(0,len(funcVar)):
            # Dependent Variables
            if funcVar[col] == 0:
                rowIndex = np.argmin(self.D[:,col+1])
                dependDict[col] = valColumn[rowIndex]
            
            #Independent Variables
            else:
                independDict[col] = 0
                
        return (valColumn[0],dependDict,independDict)

# Problem 6
def prob6(filename='productMix.npz'):
    """Solve the product mix problem for the data in 'productMix.npz'.

    Parameters:
        filename (str): the path to the data file.

    Returns:
        ((n,) ndarray): the number of units that should be produced for each product.
    """
    
    #load the data in by accessing it through its keys
    data = np.load(filename)
    A = data['A']
    p = data['p']
    m = data['m']
    d = data['d']
    
    I_x = np.eye(len(d))        # The identity matrix corresponding to demand constraints d
    
    totalCoefficients = np.vstack((A,I_x))
    totalConstraints = np.concatenate((m,d))        # The constraints corresponding to both the resource and demand constraints
    
    solver = SimplexSolver(-p,totalCoefficients,totalConstraints)           # the negative p is to make a maximization problem into a minimization problem.
    solDict = solver.solve()
    
    # Optimal number of units to ouput
    optimum = np.array([solDict[1][i] for i in range(4)])
    return optimum
