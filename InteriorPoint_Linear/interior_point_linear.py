# interior_point_linear.py
"""Volume 2: Interior Point for Linear Programs.
<Name>
<Class>
<Date>
"""

import numpy as np
from scipy import linalg as la
from scipy.stats import linregress
from matplotlib import pyplot as plt


# Auxiliary Functions ---------------------------------------------------------
def starting_point(A, b, c):
    """Calculate an initial guess to the solution of the linear program
    min c^T x, Ax = b, x>=0.
    Reference: Nocedal and Wright, p. 410.
    """
    # Calculate x, lam, mu of minimal norm satisfying both
    # the primal and dual constraints.
    B = la.inv(A @ A.T)
    x = A.T @ B @ b
    lam = B @ A @ c
    mu = c - (A.T @ lam)

    # Perturb x and s so they are nonnegative.
    dx = max((-3./2)*x.min(), 0)
    dmu = max((-3./2)*mu.min(), 0)
    x += dx*np.ones_like(x)
    mu += dmu*np.ones_like(mu)

    # Perturb x and mu so they are not too small and not too dissimilar.
    dx = .5*(x*mu).sum()/mu.sum()
    dmu = .5*(x*mu).sum()/x.sum()
    x += dx*np.ones_like(x)
    mu += dmu*np.ones_like(mu)

    return x, lam, mu

# Use this linear program generator to test your interior point method.
def randomLP(j,k):
    """Generate a linear program min c^T x s.t. Ax = b, x>=0.
    First generate m feasible constraints, then add
    slack variables to convert it into the above form.
    Parameters:
        j (int >= k): number of desired constraints.
        k (int): dimension of space in which to optimize.
    Returns:
        A ((j, j+k) ndarray): Constraint matrix.
        b ((j,) ndarray): Constraint vector.
        c ((j+k,), ndarray): Objective function with j trailing 0s.
        x ((k,) ndarray): The first 'k' terms of the solution to the LP.
    """
    A = np.random.random((j,k))*20 - 10
    A[A[:,-1]<0] *= -1
    x = np.random.random(k)*10
    b = np.zeros(j)
    b[:k] = A[:k,:] @ x
    b[k:] = A[k:,:] @ x + np.random.random(j-k)*10
    c = np.zeros(j+k)
    c[:k] = A[:k,:].sum(axis=0)/k
    A = np.hstack((A, np.eye(j)))
    return A, b, -c, x


# Problems --------------------------------------------------------------------
def interiorPoint(A, b, c, niter=20, tol=1e-16, verbose=False):
    """Solve the linear program min c^T x, Ax = b, x>=0
    using an Interior Point method.

    Parameters:
        A ((m,n) ndarray): Equality constraint matrix with full row rank.
        b ((m, ) ndarray): Equality constraint vector.
        c ((n, ) ndarray): Linear objective function coefficients.
        niter (int > 0): The maximum number of iterations to execute.
        tol (float > 0): The convergence tolerance.

    Returns:
        x ((n, ) ndarray): The optimal point.
        val (float): The minimum value of the objective function.
    """
    m = len(A)
    n = len(c)
    
    x0, lam0, mu0 = starting_point(A,b,c)
    
    # Initialize M and X diagonal matrices
    M = np.diag(mu0)
    X = np.diag(x0)
    
    
    # Define F function
    F1 = lambda x, lam, mu : A.T @ lam + mu -c
    F2 = lambda x, lam, mu : A @ x - b
    F3 = lambda x, lam, mu : M @ x
    F = lambda x, lam, mu : np.concatenate(np.array([F1(x,lam,mu),     F2(x, lam, mu),     F3(x, lam, mu)],dtype=object),axis=None)
    
    
    for i in range(niter):
        # update M and X diagonal matrices
        M = np.diag(mu0)
        X = np.diag(x0)
        
        # Compute search direction subroutine
        v = np.dot(x0,mu0) / n           # the duality measure
        sigma = 0.1                  # the centering parameter
        
        DF = np.block([[np.zeros((n,n)),    A.T,    np.eye(n)],
                   [A,  np.zeros((m,m)),    np.zeros((m,n))],
                   [M,  np.zeros((n,m)),  X]])
        
        B = -F(x0,lam0,mu0) + np.concatenate(np.array([np.zeros(n+m), sigma * v * np.ones(n)],dtype=object),axis=None)
        lu, piv = la.lu_factor(DF)
        delta = la.lu_solve((lu,piv),B)             # compute the search direction for the equation DF delta = -F + [0 0 sigma*v*e]
        
        # break delta into parts
        x0Delta = delta[:n]
        lam0Delta = delta[n:n+m]
        mu0Delta = delta[n+m:2*n+m]
        
        # Compute the minimum available step lengths for x and mu
        if np.all(mu0Delta >= 0):
            alphaMax = 1
        else:
            maskMu = mu0Delta < 0
            
            newDelta = mu0Delta[maskMu]
            newMu = mu0[maskMu]
            
            alphaMax = np.min(-newMu/newDelta)
            
        if np.all(x0Delta >= 0):
            lcDeltaMax = 1
        else:
            maskX = x0Delta < 0
            
            newDelta = x0Delta[maskX]
            newX = x0[maskX]
        
            lcDeltaMax = np.min(-newX/newDelta)
            
        alpha = min(1,0.95*alphaMax)
        lcDelta = min(1,0.95*lcDeltaMax)
        
        # Compute the next steps, and update
        x0 = x0 + lcDelta * x0Delta
        lam0 = lam0 + alpha * lam0Delta
        mu0 = mu0 + alpha * mu0Delta
        
        # Check if duality measure is within tolerance
        if v < tol:
            break
        
    return x0, c.T @ x0


def leastAbsoluteDeviations(filename='simdata.txt'):
    """Generate and show the plot requested in the lab."""
    y = []
    x = []
    data = []
    
    # Read in the data
    with open(filename,'r') as file:
        for line in file:
            line = line.split()
            line = [float(s) for s in line]
            data.append(line)
    
    data = np.array(data)
    m = data.shape[0]
    n = data.shape[1]-1
    c = np.zeros(3*m + 2*(n + 1))
    c[:m] = 1
    y = np.empty(2*m)
    y[::2] = -data[:,0]
    y[1::2] = data[:,0]
    x = data[:,1:]
    
    A = np.ones((2*m, 3*m + 2*(n + 1)))
    A[::2, :m] = np.eye(m)
    A[1::2, :m] = np.eye(m)
    A[::2, m:m+n] = -x
    A[1::2, m:m+n] = x
    A[::2, m+n:m+2*n] = x
    A[1::2, m+n:m+2*n] = -x
    A[::2, m+2*n] = -1
    A[1::2, m+2*n+1] = -1
    A[:, m+2*n+2:] = -np.eye(2*m, 2*m)
    
    sol = interiorPoint(A, y, c, niter=10)[0]
    
    beta = sol[m:m+n] - sol[m+n:m+2*n]
    b = sol[m+2*n] - sol[m+2*n+1]
    
    plt.plot(data[:,1],data[:,0],label='data')
    
    #plot least squares
    slope, intercept = linregress(data[:,1],data[:,0])[:2]
    domain = np.linspace(0,10,200)
    plt.plot(domain, domain*slope + intercept,label="least squares")
    
    # plot LAD
    plt.plot(domain, domain*beta + b,label='least absolute deviations')
    plt.legend()
    plt.title("Least squares vs LAD")
    
    plt.show()