# regular_expressions.py
"""Volume 3: Regular Expressions.
<Name> Brian James
<Class>
<Date>
"""

import re

# Problem 1
def prob1():
    """Compile and return a regular expression pattern object with the
    pattern string "python".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    
    return re.compile("python")

# Problem 2
def prob2():
    """Compile and return a regular expression pattern object that matches
    the string "^{@}(?)[%]{.}(*)[_]{&}$".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    
    return re.compile(r'\^\{@\}\(\?\)\[%\]\{\.\}\(\*\)\[_\]\{&\}\$')

# Problem 3
def prob3():
    """Compile and return a regular expression pattern object that matches
    the following strings (and no other strings).

        Book store          Mattress store          Grocery store
        Book supplier       Mattress supplier       Grocery supplier

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    
    return re.compile(r'^(Book|Mattress|Grocery) (store|supplier)$')

# Problem 4
def prob4():
    """Compile and return a regular expression pattern object that matches
    any valid Python identifier.

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    
    return re.compile(r'^[a-zA-Z_]\w*\s*$')

# Problem 5
def prob5(code):
    """Use regular expressions to place colons in the appropriate spots of the
    input string, representing Python code. You may assume that every possible
    colon is missing in the input string.

    Parameters:
        code (str): a string of Python code without any colons.

    Returns:
        (str): code, but with the colons inserted in the right places.
    """
    
    pattern_str = r'^\s*((if|elif|for|while|try|with|def|class) .+)|(else|finally)|(except .+|except)$'
    
    pattern = re.compile(pattern_str,re.MULTILINE)
    return pattern.sub(r'\1\3\4:',code)


# Problem 6
def prob6(filename="fake_contacts.txt"):
    """Use regular expressions to parse the data in the given file and format
    it uniformly, writing birthdays as mm/dd/yyyy and phone numbers as
    (xxx)xxx-xxxx. Construct a dictionary where the key is the name of an
    individual and the value is another dictionary containing their
    information. Each of these inner dictionaries should have the keys
    "birthday", "email", and "phone". In the case of missing data, map the key
    to None.

    Returns:
        (dict): a dictionary mapping names to a dictionary of personal info.
    """
    
    # open the txt file and save it as a list of strings. One person is one element of the list
    with open(filename,'r') as file:
        data = file.readlines()
        
    # create a dictionary saving each person as a key. Values will be dictionarys of contact info
    personDict = {}
    
    for line in data:
        # do regex for line to extract the right information
        patternName = re.compile(r'^[A-Z][a-zA-Z]* ([A-Z]. )?[A-Z][a-z]*\b')
        #patternBirthdate = re.compile(r'\b((\d\d)|(\d))\/((\d\d)|(\d))\/((\d\d\d\d)|(\d\d))\b')     # we have groupings so we can prepend 0s for single digits
        patternBirthdate = re.compile(r'\S*\/.*\/\S*')
        patternPhoneNum = re.compile(r'\S*-\S*')    # get all types of phone numbers. Will need to parse to unify format. 4 types of numbers
        patternEmail = re.compile(r'\S*@\S*')
        

        name = patternName.search(line)[0]     #string since name is guaranteed
        birthdate = patternBirthdate.findall(line)  #list
        phoneNum = patternPhoneNum.findall(line)    #list
        email = patternEmail.findall(line)  #list
        
        if len(birthdate) == 0:
            birthdate = None
        else:
            birthdate = birthdate[0]
            # format birthdate
            if birthdate[1] == '/':
                birthdate = '0' + birthdate
            if birthdate [4] == '/':
                birthdate = birthdate[:3] + '0' + birthdate[3:]
            if len(birthdate) != 10:
                birthdate = birthdate[:6] + '20' + birthdate[6:]        # prepend '20' to the year entry
            
            
        if len(phoneNum) == 0:
            phoneNum = None
        else:
            phoneNum = phoneNum[0]
            # format phoneNum
            if phoneNum[:2] == '1-':                            # 1-XXX-XXX-XXXX
                areaCode = phoneNum[2:5]
                phoneNum = '(' + areaCode + ')' + phoneNum[6:]
            elif phoneNum[0] == '(' and phoneNum[5] == '-':     # (XXX)-XXX-XXXX
                phoneNum = phoneNum[:5] + phoneNum[6:]
            elif phoneNum[3] == '-':                            # XXX-XXX-XXXX
                phoneNum = '(' + phoneNum[:3] + ')' + phoneNum[4:]
            # else case is phoneNum = '(XXX)XXX-XXXX
                
            
        if len(email) == 0:
            email = None
        else:
            email = email[0]
            # no need to format email
        
        # create a dictionary to be as a value in personDict. Keys are 'birthday', 'email', 'phone'
        infoDict = {"birthday": birthdate, "email": email, "phone": phoneNum}
        
        personDict[name] = infoDict
        
    return personDict