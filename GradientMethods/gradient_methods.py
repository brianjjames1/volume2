# gradient_methods.py
"""Volume 2: Gradient Descent Methods.
<Name> Brian James
<Class> Volume 2
<Date>
"""

import numpy as np
from scipy import optimize as opt
from scipy.optimize import rosen, rosen_der
from matplotlib import pyplot as plt


# Problem 1
def steepest_descent(f, Df, x0, tol=1e-5, maxiter=100):
    """Compute the minimizer of f using the exact method of steepest descent.

    Parameters:
        f (function): The objective function. Accepts a NumPy array of shape
            (n,) and returns a float.
        Df (function): The first derivative of f. Accepts and returns a NumPy
            array of shape (n,).
        x0 ((n,) ndarray): The initial guess.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        ((n,) ndarray): The approximate minimum of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    conv = False
    
    alpha_f = lambda a : f(x0-a*Df(x0))
    
    for k in range(maxiter):        # loop up to maxiter times and approximate x*
        a_k = opt.minimize_scalar(alpha_f).x      # find the optimal step size
        x1 = x0-a_k*Df(x0)
        x0 = x1
        if np.linalg.norm(Df(x0),ord=np.inf) < tol:
            conv = True
            break
    return x0,conv,k


# Problem 2
def conjugate_gradient(Q, b, x0, tol=1e-4):
    """Solve the linear system Qx = b with the conjugate gradient algorithm.

    Parameters:
        Q ((n,n) ndarray): A positive-definite square matrix.
        b ((n, ) ndarray): The right-hand side of the linear system.
        x0 ((n,) ndarray): An initial guess for the solution to Qx = b.
        tol (float): The convergence tolerance.

    Returns:
        ((n,) ndarray): The solution to the linear system Qx = b.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    n = len(b)
    
    r_0 = np.matmul(Q,x0) - b
    d_0 = -r_0
    k = 0
    
    conv = False
    
    while np.linalg.norm(r_0,ord=np.inf) >= tol and k < n:          # loop up to n times or until the norm is within tolerance
        a_k = np.dot(r_0,r_0) / np.matmul(np.matmul(d_0,Q),d_0)
        x1 = x0 + a_k*d_0
        r_1 = r_0 + a_k*np.matmul(Q,d_0)
        b_1 = np.matmul(r_1,r_1) / np.matmul(r_0,r_0)
        d_1 = -r_1 + b_1*d_0
        k += 1
        x0 = x1
        r_0 = r_1
        d_0=d_1
    
    if k <= n:
        conv = True
    
    return x0,conv,k


# Problem 3
def nonlinear_conjugate_gradient(f, df, x0, tol=1e-5, maxiter=100):
    """Compute the minimizer of f using the nonlinear conjugate gradient
    algorithm.

    Parameters:
        f (function): The objective function. Accepts a NumPy array of shape
            (n,) and returns a float.
        Df (function): The first derivative of f. Accepts and returns a NumPy
            array of shape (n,).
        x0 ((n,) ndarray): The initial guess.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        ((n,) ndarray): The approximate minimum of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    alpha = lambda a : f(x0+a*d_0)
    
    r_0 = - df(x0)
    d_0 = r_0
    a_0 = opt.minimize_scalar(alpha).x
    x1 = x0+a_0*d_0
    xk = x1
    k = 1
    
    while np.linalg.norm(r_0,ord=np.inf) >= tol and k < maxiter:            # loop through continually approximating the minimizer
        r_k = -df(xk)
        b_k = np.dot(r_k,r_k) / np.dot(r_0,r_0)
        d_k = r_k + b_k*d_0
        
        alpha_k = lambda a : f(xk+a*d_k)
        a_k = opt.minimize_scalar(alpha_k).x
        
        xk1 = xk+a_k*d_k
        k += 1
        
        xk = xk1
        
        r_0 = r_k
        
        d_0 = d_k
        
    conv = False
    if k < maxiter:         # if k does not exceed iter, then the algorithm converged
        conv = True
    return xk, conv, k


# Problem 4
def prob4(filename="linregression.txt",
          x0=np.array([-3482258, 15, 0, -2, -1, 0, 1829])):
    """Use conjugate_gradient() to solve the linear regression problem with
    the data from the given file, the given initial guess, and the default
    tolerance. Return the solution to the corresponding Normal Equations.
    """
    
    data = np.loadtxt(filename)         # load the data
    b = data[:,0]                       # extract the first column which is the y_i
    m = len(b)
    A = np.hstack((np.ones((m,1)),data[:,1:]))          # create the A matrix with a column of 1 before the rest of the data is loaded
    
    Q = A.T @ A
    
    sol = conjugate_gradient(Q,A.T@b,x0)                # compute the solution using the conjugate_gradient method
    return sol[0]

# Problem 5
class LogisticRegression1D:
    """Binary logistic regression classifier for one-dimensional data."""

    def fit(self, x, y, guess):
        """Choose the optimal beta values by minimizing the negative log
        likelihood function, given data and outcome labels.

        Parameters:
            x ((n,) ndarray): An array of n predictor variables.
            y ((n,) ndarray): An array of n outcome variables.
            guess (array): Initial guess for beta.
        """
        
        neg_likelihood = lambda b : sum(np.log(1+np.exp(-(b[0]+b[1]*x)))+(1-y)*(b[0]+b[1]*x))       # define the negative likelihood function
        minimum_b = opt.fmin_cg(neg_likelihood,guess)       # use scipy to find the optimizer for the neg_likelihood function
        self.b_min = minimum_b              # store the minimum beta values as attributes

    def predict(self, x):
        """Calculate the probability of an unlabeled predictor variable
        having an outcome of 1.

        Parameters:
            x (float): a predictor variable with an unknown label.
        """
        
        sigma_x = 1 / (1+np.exp(-(self.b_min[0]+self.b_min[1]*x)))          # compute the probability with this simple formula
        
        return sigma_x


# Problem 6
def prob6(filename="challenger.npy", guess=np.array([20., -1.])):
    """Return the probability of O-ring damage at 31 degrees Farenheit.
    Additionally, plot the logistic curve through the challenger data
    on the interval [30, 100].

    Parameters:
        filename (str): The file to perform logistic regression on.
                        Defaults to "challenger.npy"
        guess (array): The initial guess for beta.
                        Defaults to [20., -1.]
    """
    
    data = np.load(filename)
    x = data[:,0]
    y = data[:,1]
    
    challengerLogReg = LogisticRegression1D()
    challengerLogReg.fit(x,y,guess)
    
    predicted = challengerLogReg.predict(31)
    
    inputSpace = np.linspace(30,100,500)
    output = challengerLogReg.predict(inputSpace)
    
    plt.plot(inputSpace,output,color='orange')              # plot all the points of x in [30,100]
    plt.plot(x,y,'o',label='Previous Damage')               # plot the given data
    plt.plot(31,predicted,'go',label="P(Damage) at Launch") # plot the probability of O-ring damage at 31 degrees Fahrenheit
    
    """Add details to the graph"""
    plt.title("Probability of O-Ring Damage")
    plt.ylabel("O-Ring Damage")
    plt.xlabel("Temperature")
    plt.legend()
    plt.show()
    
    return predicted
    
    
prob6()
