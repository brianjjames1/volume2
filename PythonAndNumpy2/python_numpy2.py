# python_intro.py
"""Python Essentials: Introduction to Python.
<Name>
<Class>
<Date>
"""

import numpy as np

#Problem 1
def isolate(a, b, c, d, e):
    print(1,2,3,sep="   ",end=' ')
    print(4,5)
    raise NotImplementedError("Problem 1 Incomplete")

#Problem 2
def first_half(string):
    length = int(len(string)/2)
    return string[:length]
    raise NotImplementedError("Problem 2 Incomplete")


def backward(first_string):
    return first_string[::-1]
    raise NotImplementedError("Problem 2 Incomplete")

#Problem 3
def list_ops():
    my_list = ["bear", "ant", "cat", "dog"]
    my_list.append("eagle")
    my_list.remove("cat")
    my_list.insert(2, "fox")
    my_list.pop(1)
    my_list.sort(reverse=True)
    print(my_list)
    
    raise NotImplementedError("Problem 3 Incomplete")

#Problem 4
def alt_harmonic(n):
    """Return the partial sum of the first n terms of the alternating
    harmonic series. Use this function to approximate ln(2).
    """
    return sum([(-1)**(n+1)/n for n in range(1,n+1)])
    
    raise NotImplementedError("Problem 4 Incomplete")



def prob5(A):
    """Make a copy of 'A' and set all negative entries of the copy to 0.
    Return the copy.

    Example:
        >>> A = np.array([-3,-1,3])
        >>> prob4(A)
        array([0, 0, 3])
    """
    
    B = np.copy(A)
    mask = B < 0
    B[mask] = 0
    return B
    
    raise NotImplementedError("Problem 5 Incomplete")

def prob6():
    """Define the matrices A, B, and C as arrays. Return the block matrix
                                | 0 A^T I |
                                | A  0  0 |,
                                | B  0  C |
    where I is the 3x3 identity matrix and each 0 is a matrix of all zeros
    of the appropriate size.
    """
    
    A = np.array([[0,2,4],[1,3,5]])
    B = np.tril(np.full((3,3),3))
    C = np.diag([-2,-2,-2])
    
    row1 = [0,A.T,np.eye(3,dtype=int)]
    row2 = [A,0,0]
    row3 = [B,0,C]
    final_array = np.vstack((row1,row2,row3))
    print(final_array)
    return final_array
    
    
    raise NotImplementedError("Problem 6 Incomplete")

def prob7(A):
    """Divide each row of 'A' by the row sum and return the resulting array.

    Example:
        >>> A = np.array([[1,1,0],[0,1,0],[1,1,1]])
        >>> prob6(A)
        array([[ 0.5       ,  0.5       ,  0.        ],
               [ 0.        ,  1.        ,  0.        ],
               [ 0.33333333,  0.33333333,  0.33333333]])
    """

    A = A.astype(float)
    
    row_sum = A.sum(axis=1)
    row_count = 0
    for i in A:
        col_count = 0
        for j in i:
           A[row_count,col_count] = j / row_sum[row_count]
           col_count = col_count + 1
        row_count = row_count + 1
           
    return(A)
    
    raise NotImplementedError("Problem 7 Incomplete")


def prob8():
    """Given the array stored in grid.npy, return the greatest product of four
    adjacent numbers in the same direction (up, down, left, right, or
    diagonally) in the grid.
    """
    
    grid = np.load("grid.npy")
    print(grid[:,:-3])
    horizontal_max = np.max(grid[:,:-3] * grid[:,1:-2] * grid[:,2:-1] * grid[:,3:])
    vertical_max = np.max(grid[:-3,:] * grid[1:-2,:] * grid[2:-1,:] * grid[3:,:])
    right_diag_max = np.max(grid[:-3,:-3] * grid[1:-2,1:-2] * grid[2:-1,2:-1] * grid[3:,3:])
    left_diag_max = np.max(grid[3:,:-3] * grid[2:-1,1:-2] * grid[1:-2,2:-1] * grid[:-3,3:])
    winner = max(horizontal_max,vertical_max,right_diag_max,left_diag_max)    
    
    return winner
    
    raise NotImplementedError("Problem 8 Incomplete")


