# breadth_first_search.py
"""Volume 2: Breadth-First Search.
<Name> Brian James
<Class> Sec 03
<Date> November 3, 2021
"""
import collections
import networkx as nx
from matplotlib import pyplot as plt

# Problems 1-3
class Graph:
    """A graph object, stored as an adjacency dictionary. Each node in the
    graph is a key in the dictionary. The value of each key is a set of
    the corresponding node's neighbors.

    Attributes:
        d (dict): the adjacency dictionary of the graph.
    """
    def __init__(self, adjacency={}):
        """Store the adjacency dictionary as a class attribute"""
        self.d = dict(adjacency)

    def __str__(self):
        """String representation: a view of the adjacency dictionary."""
        return str(self.d)

    # Problem 1
    def add_node(self, n):
        """Add n to the graph (with no initial edges) if it is not already
        present.

        Parameters:
            n: the label for the new node.
        """
        try:
            if isinstance(list(self.d.keys()).index(n),int):    # Node n is found
                return
        except ValueError:      # Node n is not found, so add to graph
            self.d.update({n: set()})

    # Problem 1
    def add_edge(self, u, v):
        """Add an edge between node u and node v. Also add u and v to the graph
        if they are not already present.

        Parameters:
            u: a node label.
            v: a node label.
        """
        try:
            if list(self.d.keys()).index(u) >= 0:    # if u is a node
                u_found = True
        except ValueError:      # if u is not a node
            u_found = False
        
        try:
            if list(self.d.keys()).index(v) >= 0:    # if v is a node
                v_found = True
        except ValueError:      # if v is not a node
            v_found = False
            
        if u_found:
            self.d[u].add(v)
        if v_found:
            self.d[v].add(u)
        if not u_found:
            self.add_node(u)
            self.d[u].add(v)
        if not v_found:
            self.add_node(v)
            self.d[v].add(u)

    # Problem 1
    def remove_node(self, n):
        """Remove n from the graph, including all edges adjacent to it.

        Parameters:
            n: the label for the node to remove.

        Raises:
            KeyError: if n is not in the graph.
        """
        try:
            if list(self.d.keys()).index(n) >= 0:  # if node n is in graph
                for key, value in self.d.items():   # delete all edges containing n
                    try:
                        value.remove(n)
                    except KeyError:
                        pass
                self.d.pop(n)   # remove node n 
        except ValueError:  # if node n is not in graph
            raise KeyError

    # Problem 1
    def remove_edge(self, u, v):
        """Remove the edge between nodes u and v.

        Parameters:
            u: a node label.
            v: a node label.

        Raises:
            KeyError: if u or v are not in the graph, or if there is no
                edge between u and v.
        """
        try:
            if list(self.d.keys()).index(u) >= 0 and list(self.d.keys()).index(v) >=0:  # if both nodes u and v are in graph
                pass
                # check if an edge exists between the two
                u_v_found = False
                for element in self.d[u]:
                    if v == element:
                        u_v_found = True
                        break
                v_u_found = False
                for element in self.d[v]:
                    if u == element:
                        v_u_found = True
                        break
                if u_v_found:
                    self.d[u].remove(v)
                if v_u_found:
                    self.d[v].remove(u)
                if not u_v_found and not v_u_found:
                    raise KeyError
        except ValueError:  # u or v are not found in the graph
            raise KeyError

    # Problem 2
    def traverse(self, source):
        """Traverse the graph with a breadth-first search until all nodes
        have been visited. Return the list of nodes in the order that they
        were visited.

        Parameters:
            source: the node to start the search at.

        Returns:
            (list): the nodes in order of visitation.

        Raises:
            KeyError: if the source node is not in the graph.
        """
        try:
            if list(self.d.keys()).index(source) >= 0:
        
                V = []  # nodes that have already been visited
                Q = collections.deque() # Nodes that will be visited according to the order they were discovered (append to right, pop left)
                M = set()   # nodes that have been visited or marked to be visited
                Q.append(source)
                M.add(source)
                
                while len(Q) > 0:
                    current_node = Q.popleft()  # the visiting node
                    V.append(current_node)
                    neighbors = self.d[current_node] - M
                    for node in neighbors:
                        Q.append(node)
                    M.update(neighbors)
                return V
        except ValueError:  # source node not found
            raise KeyError

    # Problem 3
    def shortest_path(self, source, target):
        """Begin a BFS at the source node and proceed until the target is
        found. Return a list containing the nodes in the shortest path from
        the source to the target, including endoints.

        Parameters:
            source: the node to start the search at.
            target: the node to search for.

        Returns:
            A list of nodes along the shortest path from source to target,
                including the endpoints.

        Raises:
            KeyError: if the source or target nodes are not in the graph.
        """
        try:
            if list(self.d.keys()).index(source) >= 0 and list(self.d.keys()).index(target) >= 0:
                
                pathdict = dict()
                V = []  # nodes that have already been visited
                Q = collections.deque() # Nodes that will be visited according to the order they were discovered (append to right, pop left)
                M = set()   # nodes that have been visited or marked to be visited
                Q.append(source)
                M.add(source)
                
                while len(Q) > 0:
                    current_node = Q.popleft()  # the visiting node
                    V.append(current_node)
                    if current_node == target: # if target node is found, break out of loop and calculate visit path
                        break
                    neighbors = self.d[current_node] - M
                    for node in neighbors:
                        Q.append(node)
                        pathdict.update({node:current_node})
                    M.update(neighbors)
                    
                #Find visit path
                def _findpath(node, predecessor, pathlist):
                    if predecessor == source:
                        pathlist.append(predecessor)
                        return
                    else:
                        _findpath(predecessor,pathdict[predecessor],pathlist)
                        pathlist.append(predecessor)
                        return
                visitedpath = []
                _findpath(target,pathdict[target],visitedpath)
                visitedpath.append(target)
                return visitedpath
                
        except ValueError:  # source node not found
            raise KeyError


# Problems 4-6
class MovieGraph:
    """Class for solving the Kevin Bacon problem with movie data from IMDb."""

    # Problem 4
    def __init__(self, filename="movie_data.txt"):
        """Initialize a set for movie titles, a set for actor names, and an
        empty NetworkX Graph, and store them as attributes. Read the speficied
        file line by line, adding the title to the set of movies and the cast
        members to the set of actors. Add an edge to the graph between the
        movie and each cast member.

        Each line of the file represents one movie: the title is listed first,
        then the cast members, with entries separated by a '/' character.
        For example, the line for 'The Dark Knight (2008)' starts with

        The Dark Knight (2008)/Christian Bale/Heath Ledger/Aaron Eckhart/...

        Any '/' characters in movie titles have been replaced with the
        vertical pipe character | (for example, Frost|Nixon (2008)).
        """
        self.G = nx.Graph()
        self.movie_title = set()
        self.actor_names = set()
        
        with open(filename) as file:
            for line in file:
                split_line = line.split('/')    # index 0 is movie title, rest is names
                self.movie_title.add(split_line[0])
                self.actor_names.update(split_line[1:])
                
                movie_actor_tuples = [(split_line[0],name) for name in split_line[1:]]
                self.G.add_edges_from(movie_actor_tuples)

    # Problem 5
    def path_to_actor(self, source, target):
        """Compute the shortest path from source to target and the degrees of
        separation between source and target.

        Returns:
            (list): a shortest path from source to target, including endpoints and movies.
            (int): the number of steps from source to target, excluding movies.
        """
        shortest_path = nx.shortest_path(self.G,source,target)
        return shortest_path, (len(shortest_path)-1)/2

    # Problem 6
    def average_number(self, target):
        """Calculate the shortest path lengths of every actor to the target
        (not including movies). Plot the distribution of path lengths and
        return the average path length.

        Returns:
            (float): the average path length from actor to target.
        """
        
        path_dict = dict(nx.single_target_shortest_path_length(self.G,target))
        path_lengths = list(path_dict.values())
        actor_lengths = [e for e in path_lengths if e & 2 == 0]     # only keep even path lengths because those are actors
        actor_lengths = [e/2 for e in path_lengths] # divide by two to count steps removed (movies not included)
        average_lengths = sum(actor_lengths) / len(actor_lengths)
        
        plt.hist(actor_lengths,bins=[i-.5 for i in range(8)])
        plt.xlabel("Number of steps removed")
        plt.ylabel("Frequency")
        plt.title("Shortest path lengths to " + str(target))
        plt.show()

        return average_lengths