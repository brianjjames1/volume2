#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 21:44:33 2021

@author: James
"""

# Exercises 7.1 - 7.5 (entire assignment)


# Exercise 7.1
def exercise_1():
    """This function (i) plots the p.d.f. of the random variable X := Z^2,
    where Z ~ N(0,1) is standard normal. It also (ii) returns the c.d.f. F_X(x)
    for x = {0.5, 1.0, 1.5}. (iii) it computes the expected value E(X).
    Finally, it (iv) computes the variance Var(X).

    Each of (i)-(iv) will be calculated 4 times. Three times with Monte Carlo
    methods (part A) (for 10^2, 10^4, and 10^6 samples), and again with built
    -in functions (part B) from your preferred computational tools (I like
    scipy.stats, but maybe you prefer numpy.random, or something else).

    Please return 3 variables:
        1 - the c.d.f. of F_X(x) at each x-value
        2 - E(X)
        3 - Var(X)
    as lists for each of the 4 calculations you need.

    Example output (using completely made-up numbers):
        #     (A) k = 2         (A) k = 4          (A) k = 6                   (B)
        [[0.1, 0.25, 0.75], [0.12, 0.26, 0.8], [0.125, 0.275, 0.787], [0.125, 0.275, 0.787]],
        [1.1, 1.24, 1.2534, 1.2533],
        [2.1, 2.23, 2.234, 2.235]

    """
    # Implement your code here!
    raise NotImplementedError("Still need to finish Exercise 7.1.")


# Exercise 7.2
def exercise_2(k):
    """Approximate pi using the Monte Carlo method from the reading, using
    10^k samples.

    Returns:
        pi_estimate (float) : your estimated value for pi.
        standard_error (float) : the (approx.) standard error for pi_estimate.
    """
    raise NotImplementedError("Haven't approximated pi yet.")


# Exercise 7.3
def exercise_3():
    """Return the (approximate) standard errors for both of the methods in the
    exercise (the order should be the s.e. for (i), then (ii)). Don't forget to
    comment your code! I'll be checking how you implemented both methods.
    """
    raise NotImplementedError("You are a master of Monte Carlo integration")


# Exercise 7.4
def exercise_4():
    """Return the probability estimate and the number of samples required."""
    raise NotImplementedError("You've made it this far, nice job!")


# Exercise 7.5
def exercise_5():
    """Return the probability of having negative winnings, and the number of
    samples you used. Also, print your justification for the number of samples
    you used.
    """
    justification = """You're welcome to put your justification here.
    It can even cover multiple lines if you want."""
    print(justification)

    raise NotImplementedError("I'm happy you're deleting this error. That means you're almost done!")


if __name__ == "__main__":
    # You can run test code in this block
    pass