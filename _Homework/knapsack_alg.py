#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 02:38:07 2021

@author: James

To use this program, use ipython and call the knapsack method with the proper parameters.

"""


def knapsack(weight, item_list):
    """
    Parameters
    ----------
    weight : float
        the maximum weight the knapsack can carry
    item_list : list of tuples(item_weight (float), item_value (float))
        DESCRIPTION.

    Returns
    -------
    The maximum value that can be carried in the knapsack

    """
    list_size = len(item_list)
    #remaining_weight = weight  # initialize carrying weight
    current_value = 0 # initialize value of the bag
    best_value = 0
    remaining_weight = weight
    best_contents = []
    
    for i in range(list_size):
        bag_contents = []
        if item_list[i][0] <= remaining_weight:
            bag_contents.append(i)
            if len(item_list[i:]) == 1:
                current_value = item_list[i][1]
            else:
                current_value, bag_contents = knapsack_helper(item_list[i+1:], remaining_weight-item_list[i][0], item_list[i][1], best_value,bag_contents,i+1)
        if current_value > best_value:
            best_value = current_value
            best_contents = bag_contents
    
    return "Max value: " + str(best_value) + ", Item list" + str(best_contents)

def knapsack_helper(item_list, remaining_weight, current_value, best_value, bag_contents,index):
    if len(item_list) == 0:
        return current_value, bag_contents
    """
    if len(item_list) == 1:     # Add the last item if you can and see what the value of the knapsack is
        if item_list[0][0] <= remaining_weight:
            current_value = current_value + item_list[0][1]
            return current_value
    """
        
    
    list_size = len(item_list)
    temp_value = current_value
    best_contents = bag_contents
    
    for i in range(list_size):
        # Check if you can add item i
        if item_list[i][0] <= remaining_weight:
            bag_contents.append(i+index)
            temp_value, bag_contents = knapsack_helper(item_list[i+1:], remaining_weight-item_list[i][0],current_value+item_list[i][1], best_value, bag_contents,index+i+1)
        if temp_value > best_value:
            best_value = temp_value
            best_contents = bag_contents
    
    return best_value, best_contents

"""
my_list = [(20,0.5),(100,1),(50,0.75)]
weight = 100

knapsack_alg(weight,my_list)
"""
    
    