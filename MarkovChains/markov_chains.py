# markov_chains.py
"""Volume 2: Markov Chains.
<Name> Brian James
<Class> 03
<Date>
"""

import numpy as np
from scipy import linalg as la


class MarkovChain:
    """A Markov chain with finitely many states.

    Attributes:
        trans_matrix ((n,n), ndarray): the column-stochastic transition matrix for a
        Markov chain with n states.
        state_labels (list(str)): the labels for all the states in the transition matrix
        
    """
    # Problem 1
    def __init__(self, A, states=None):
        """Check that A is column stochastic and construct a dictionary
        mapping a state's label to its index (the row / column of A that the
        state corresponds to). Save the transition matrix, the list of state
        labels, and the label-to-index dictionary as attributes.

        Parameters:
        A ((n,n) ndarray): the column-stochastic transition matrix for a
            Markov chain with n states.
        states (list(str)): a list of n labels corresponding to the n states.
            If not provided, the labels are the indices 0, 1, ..., n-1.

        Raises:
            ValueError: if A is not square or is not column stochastic.

        Example:
            >>> MarkovChain(np.array([[.5, .8], [.5, .2]], states=["A", "B"])
        corresponds to the Markov Chain with transition matrix
                                   from A  from B
                            to A [   .5      .8   ]
                            to B [   .5      .2   ]
        and the label-to-index dictionary is {"A":0, "B":1}.
        """
        m,n = np.shape(A)
        if m != n:  # check if matrix is square
            raise ValueError("Matrix must be square")
        col_stoch = True    # initialize col_stoch bool for the following for-loop
        for i in range(n):
            if not np.isclose(sum(A[:,i]),1,rtol=1e-5): # if the columns don't add to 1, mark the matrix as not col stochastic and break
                col_stoch = False
                break
        if not col_stoch:
            raise ValueError("Matrix must be column-stochastic")
        else:
            self.state_dic = dict()
            if states == None:
                for i in range(n):  # create labels named after integers
                    self.state_dic[i] = i
            else:
                for i in range(len(states)):    # create labels after the states list
                    self.state_dic[states[i]] = i
        self.trans_matrix = A
        self.state_labels = states

    # Problem 2
    def transition(self, state):
        """Transition to a new state by making a random draw from the outgoing
        probabilities of the state with the specified label.

        Parameters:
            state (str): the label for the current state.

        Returns:
            (str): the label of the state to transitioned to.
        """
        col_index = self.state_dic[state]
        results = np.random.multinomial(1,self.trans_matrix[:,col_index])
        new_index = np.argmax(results)
        return self.state_labels[new_index]

    # Problem 3
    def walk(self, start, N):
        """Starting at the specified state, use the transition() method to
        transition from state to state N-1 times, recording the state label at
        each step.

        Parameters:
            start (str): The starting state label.

        Returns:
            (list(str)): A list of N state labels, including start.
        """
        state_list = [start]
        current = start
        for i in range(N-1):
            current = self.transition(current)
            state_list.append(current)
        return state_list

    # Problem 3
    def path(self, start, stop):
        """Beginning at the start state, transition from state to state until
        arriving at the stop state, recording the state label at each step.

        Parameters:
            start (str): The starting state label.
            stop (str): The stopping state label.

        Returns:
            (list(str)): A list of state labels from start to stop.
        """
        state_list = [start]
        current = start
        while (current!=stop):
            current = self.transition(current)  # continually call this function until current reaches the stopping point
            state_list.append(current)
        return state_list

    # Problem 4
    def steady_state(self, tol=1e-12, maxiter=40):
        """Compute the steady state of the transition matrix A.

        Parameters:
            tol (float): The convergence tolerance.
            maxiter (int): The maximum number of iterations to compute.

        Returns:
            ((n,) ndarray): The steady state distribution vector of A.

        Raises:
            ValueError: if there is no convergence within maxiter iterations.
        """
        x_0 = np.random.rand(len(self.trans_matrix))
        x_0 = x_0/sum(x_0)  # divide by sum to ensure the elements add to 1
        
        k = 0
        x_k = x_0
        while(k < maxiter):
            x_k1 = np.dot(self.trans_matrix,x_k)
            if la.norm(x_k1-x_k,ord=1) < tol:   # if true,, the steady state distribution vector has been found
                return x_k
            k += 1
            x_k = x_k1  # iterate to the next vector
        raise ValueError("A^k does not converge")   # if you break out of the loop, there is no steady state distribution

class SentenceGenerator(MarkovChain):
    """A Markov-based simulator for natural language.

    Attributes:
        markov (MarkovChain): the Markov Chain object that contains a transition matrix and list of labels
    """
    # Problem 5
    def __init__(self, filename):
        """Read the specified file and build a transition matrix from its
        contents. You may assume that the file has one complete sentence
        written on each line.
        """
        with open(filename) as file:
            lines = file.readlines()
        words = list()
        for line in lines:
            for word in line.split():
                words.append(word) # create a list of all words
                
        labels = list(set(words) )   # Remove all duplicate words
        labels.insert(0,"$tart")
        labels.append("$top")
        transition = np.zeros((len(labels),len(labels)))
        indices_dic = dict()
        for i in range(len(labels)):
            indices_dic[labels[i]] = i      #create a dictionary that contains the index of each word in the list called 'states'
            
        for line in lines:          #Split all sentences
            line = line.split()
            line.insert(0,"$tart")  #Prepend $tart
            line.append("$top")     #Append $top
            for i in range(len(line)-1):
                current_word = line[i]
                next_word = line[i+1]
                coordinate = (indices_dic[next_word],indices_dic[current_word])
                transition[coordinate] += 1
        transition[indices_dic["$top"],indices_dic["$top"]] += 1        # Force $top to point to itself
        
        transition = transition/transition.sum(0)
        self.markov = MarkovChain(transition,labels)

    # Problem 6
    def babble(self):
        """Create a random sentence using MarkovChain.path().

        Returns:
            (str): A sentence generated with the transition matrix, not
                including the labels for the $tart and $top states.

        Example:
            >>> yoda = SentenceGenerator("yoda.txt")
            >>> print(yoda.babble())
            The dark side of loss is a path as one with you.
        """
        nonsense_talk = self.markov.path("$tart","$top")
        nonsense_talk.pop() # remove $tart from the string
        nonsense_talk.pop(0) # remove $top from the string
        return ' '.join(nonsense_talk)  # convert path list into a string separated by spaces